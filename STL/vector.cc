#include <iostream>
#include <new.h>

using namespace std;

template <class T, class Alloc = alloc>
class vector
{
 protected:
    iterator start;                 //表视当前使用空间的头
    iterator finish;                //表视当前使用空间的尾
    iterator end_of_storage;        //表示当前可用空间的尾
};