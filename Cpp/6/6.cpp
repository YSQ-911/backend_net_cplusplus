#include"stdafx.h"

#include <iostream>
#include <stack>

__declspec(dllexport) ListNode *CreateListNode(int value);
__declspec(dllexport) void ConnectListNodes(ListNode *pCurrent, ListNode *pNext);
__declspec(dllexport) void PrintListNode(ListNode *pNode);
__declspec(dllexport) void PrintList(ListNode *pHead);
__declspec(dllexport) void DestroyList(ListNode *pHead);
__declspec(dllexport) void AddToTail(ListNode **pHead, int value);
__declspec(dllexport) void RemoveNode(ListNode **pHead, int value);

//利用栈结构解决
// void PrintListReversingly_Iteratively(ListNode *pHead)
// {
//     if (pHead == nullptr)
//     {
//         return;
//     }
//     std::stack<ListNode *> re;
//     ListNode *p_Node = pHead;

//     while (p_Node != nullptr)
//     {
//         re.push(p_Node);
//         p_Node = p_Node->m_pNext;
//     }

//     while (!re.empty())
//     {
//         p_Node = re.top();
//         printf("%d", p_Node->m_nValue);
//         re.pop();
//     }
// }

//利用递归解决问题
void PrintListReversingly_Recursively(ListNode *pHead)
{
    if (pHead != nullptr)
    {
        if (pHead->m_pNext != nullptr)
        {
            PrintListReversingly_Recursively(pHead->m_pNext);
        }
        printf("%d", pHead->m_nValue);
    }
}

// ====================测试代码====================
void Test(ListNode *pHead)
{
    PrintList(pHead);
    //PrintListReversingly_Iteratively(pHead);
    printf("\n");
    PrintListReversingly_Recursively(pHead);
}

// 1->2->3->4->5
void Test1()
{
    printf("\nTest1 begins.\n");

    ListNode *pNode1 = CreateListNode(1);
    ListNode *pNode2 = CreateListNode(2);
    ListNode *pNode3 = CreateListNode(3);
    ListNode *pNode4 = CreateListNode(4);
    ListNode *pNode5 = CreateListNode(5);

    ConnectListNodes(pNode1, pNode2);
    ConnectListNodes(pNode2, pNode3);
    ConnectListNodes(pNode3, pNode4);
    ConnectListNodes(pNode4, pNode5);

    Test(pNode1);

    DestroyList(pNode1);
}

// 只有一个结点的链表: 1
void Test2()
{
    printf("\nTest2 begins.\n");

    ListNode *pNode1 = CreateListNode(1);

    Test(pNode1);

    DestroyList(pNode1);
}

// 空链表
void Test3()
{
    printf("\nTest3 begins.\n");

    Test(nullptr);
}

int main(int argc, char *argv[])
{
    Test1();
    Test2();
    Test3();
    getchar();
    return 0;
}
