// test04.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"

//作业：模拟list
//提交时间：下周周五下午2点
//直接提交h文件即可

//deque  双向队列  和vector非常相似，也是用动态数组来管理元素   
//假如有一个内存区块不再被使用的时候，可以释放
//头尾两端都可以进行快速的插入和删除
//存取元素的时候，deque的内部结构会多一个间接的过程，所以元素的存取相比vector会慢一些
//迭代器需要在不同的内存区块之间跳转，所以迭代器必须是一个智能指针
//在对内存区块有限制的系统中,deque包含的元素更多，因为不止一个内存区块
//deque 不支持对容量和内存重分配时机的控制，deque的内存重分配 要优于vector，不需要对所有的内存进行重分配

//deque和vector相似之处
//1、在中端插入，删除元素的速度相对比较慢
//2、迭代器同样都属于随机存取迭代器

//以下情况用deque
//1、需要在两端插入和删除
//2、要求容器释放不再使用的元素

//deque不提供容量的操作(reserve capacity这两个函数没有)
//deque提供push_front pop_front

//异常：at这个函数会抛异常，其它都不会去抛异常
//1、元素的插入和删除可能会导致部分内存区块重分配，所以迭代器会失效（指针，引用）
//2、唯一例外的是在头尾做插入，指针和引用仍然有效，迭代器还是会失效

#include <deque>
using std::deque;
#include <vector>
using std::vector;

//list  双向链表
#include <list>
using std::list;

//1、list不支持随机的存取，比如要找第4个元素，需要从头节点开始往后遍历4个节点，list遍历，查找元素速度比较慢
//2、list在任意位置上面进行插入和删除速度是相当快，常数时间就能完成
//3、list的插入和删除操作不会导致迭代器，指针，引用失效

//list和vector及deque做一个比较
//1、list没有at函数，也没有[]运算符
//2、list并没有容量的大小，不需要内存重分配
//3、list提供了一些特殊的成员函数

bool isBase(int i)
{
	return i % 2;
}

bool isGrather(int a, int b)
{
	return a > b;
}
#include <time.h>
int _tmain(int argc, _TCHAR* argv[])
{
	srand((size_t)time(NULL));
	list<int> l1;
	l1.pop_front();
	l1.pop_back();
	l1.push_front(12);
	for (int i = 0; i < 10; ++i)
		l1.push_back(rand() % 100);
	list<int>::iterator l1It;
	for (l1It = l1.begin(); l1It != l1.end(); l1It++)
		printf("%d   ", *l1It);
	printf("\n//============================\n");
	list<int> l2;
	for (int i = 0; i < 10; ++i)
		l2.push_back(rand() % 100);
	list<int>::iterator l2It;
	for (l2It = l2.begin(); l2It != l2.end(); l2It++)
		printf("%d   ", *l2It);
	printf("\n//============================\n");
	//l1.sort();
	l1.sort(isGrather);
	for (l1It = l1.begin(); l1It != l1.end(); l1It++)
		printf("%d   ", *l1It);
	printf("\n//============================\n");
	//l2.sort();
	l2.sort(isGrather);
	for (l2It = l2.begin(); l2It != l2.end(); l2It++)
		printf("%d   ", *l2It);
	printf("\n//============================\n");
	//l1.merge(l2);//一个参数表示默认是升序
	//改变了排序准则
	l1.merge(l2, isGrather);//l1和l2已序的情况下，把l2的元素转移到l1，并且再次保证已序（l1和l2的排序准则应该是一致的）
	for (l1It = l1.begin(); l1It != l1.end(); l1It++)
		printf("%d   ", *l1It);
	printf("\n//============================\n");
	for (l2It = l2.begin(); l2It != l2.end(); l2It++)
		printf("%d   ", *l2It);
	printf("\n//============================\n");


	//list<int> stuList;
	//for (int i = 0; i < 10; ++i)
	//	stuList.push_back(rand() % 100);
	//list<int>::iterator stuListIt;
	//for (stuListIt = stuList.begin(); stuListIt != stuList.end(); stuListIt++)
	//	printf("%d   ",*stuListIt);
	//printf("\n//============================\n");
	//stuList.reverse();//链表的反序
	//for (stuListIt = stuList.begin(); stuListIt != stuList.end(); stuListIt++)
	//	printf("%d   ", *stuListIt);
	//printf("\n//============================\n");
	//stuList.sort();//链表的默认升序
	//for (stuListIt = stuList.begin(); stuListIt != stuList.end(); stuListIt++)
	//	printf("%d   ", *stuListIt);
	//printf("\n//============================\n");
	//stuList.sort(isGrather);//给参数变成降序
	//for (stuListIt = stuList.begin(); stuListIt != stuList.end(); stuListIt++)
	//	printf("%d   ", *stuListIt);


	//list<int> li;
	//for (int i = 0; i < 10; ++i)
	//	li.push_back(i + 1);
	//li.push_front(5);
	//list<int>::iterator lit,lit2,lit3;
	//lit = li.begin();
	//for (int i = 0; i < 5; ++i)
	//{
	//	lit++;//迭代器不是随机存取迭代器，所以只能循环自增
	//}
	//for (int i = 0; i < 4; ++i)
	//{
	//	li.insert(lit, 5);
	//}
	//li.push_back(5);
	//li.push_back(1);
	////li.remove(5);//删除元素值为5的所有元素
	////li.remove_if(isBase);//删除isbase条件为真的所有元素，isbase是函数名，即函数首地址

	////li.unique();//如果存在若干相邻的且数值相同的元素，删除重复元素，只留下一个
	//li.unique(isGrather);//如果存在若干相邻且相邻元素通过isGrather函数运算之后为true,删除重复

	//lit2 = li.begin();

	//for (int i = 0; i < 6; ++i)
	//	lit2++;
	//lit3 = lit2;
	//for (int i = 0; i < 4; ++i)
	//	lit3++;

	//list<int> li1;
	//list<int>::iterator lit1 = li1.begin();
	////li1.splice(lit1, li);//把li这个容器里面的所有元素转移到了lit1的位置，li没有元素了
	////li1.splice(lit1, li, lit2);//把li这个容器里面lit2所指的元素转移到了lit1里面
	//li1.splice(lit1, li, lit2,lit3);
	//for (lit = li.begin(); lit != li.end(); ++lit)
	//	printf("%d\n",*lit);
	//printf("//==============\n");

	//for (lit1 = li1.begin(); lit1 != li1.end(); ++lit1)
	//	printf("%d\n", *lit1);

	////没有讲的函数参照vector
	//deque<int> d(10);

	//deque<int> d1(d);
	//deque<int> d2(3, 100);
	//deque<int>::iterator dit,dit1;
	//dit = d2.begin();
	//dit1 = d2.end();
	//deque<int> d3(dit, dit1);
	//d3.size();
	//d3.empty();
	//d3 == d2;
	//d3.at(1);
	//d3[2];
	//d3.front();
	//d3.back();
	//d2 = d3;
	//d2.assign(10,123);
	//d2.swap(d);
	//swap(d, d2);
	//dit = d2.begin() + 1;
	//d2.insert(dit, 100);
	//d2.push_back(12);
	//d2.pop_back();
	//d2.push_front(34);
	//d2.pop_front();
	//dit = d2.begin();
	//d2.erase(dit);
	//d2.resize(19);

	return 0;
}

