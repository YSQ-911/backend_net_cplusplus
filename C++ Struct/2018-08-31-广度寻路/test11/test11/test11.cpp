// test11.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <vector>
using std::vector;

//广度寻路  广度优先搜索  bfs
//每一次去搜索指点的节点，并将其所有未访问过的邻近节点加入到搜索队列中，这个队列可以理解为树的下一层

//1、走直线，不能走斜线
//2、可以找到所有的路径，在一定范围之内比较优化
//3、如果有路通往终点，广度找到的必然是最短路径

#define MAP_ROW 6
#define MAP_COL 8

enum PathDir{p_up,p_down,p_left,p_right};

struct MyPoint
{
	int row, col;
};

struct PathData
{
	PathDir dir;
	int val;
	bool isFind;
};

struct MyPathNode//树形结构的节点类型
{

	MyPoint pos;//数据域，记录寻找的可通行坐标
	MyPathNode *parent;//父节点指针，当前的这个节点的父亲是谁
	vector<MyPathNode *> pChildNode;//动态的指针数组，用来保存当前节点的所有子节点的地址
};

bool isCheckPoint(MyPoint const& point, PathData pArr[][MAP_COL])
{
	//判断，是否在二维数组范围（有没有越界）
	if (point.row >= 0 && point.row < MAP_ROW && point.col >= 0 && point.col < MAP_COL)
	{
		//条件满足，这个点没有越界
		if (pArr[point.row][point.col].isFind == false//判断这个点没有被访问过
			&& pArr[point.row][point.col].val == 0)//判断这个点可以通行
			return true;
	}
	return false;
}

void clearTree(MyPathNode *& root)
{
	if (root)
	{
		for (size_t i = 0; i < root->pChildNode.size(); ++i)
		{
			clearTree(root->pChildNode[i]);
		}
		delete root;
		root = NULL;
	}
}

int _tmain(int argc, _TCHAR* argv[])
{
	int arr[MAP_ROW][MAP_COL] = {
		{ 0, 0, 1, 0, 0, 0, 0, 0 },
		{ 0, 0, 1, 0, 0, 0, 0, 0 },
		{ 0, 1, 0, 0, 0, 0, 0, 0 },
		{ 0, 1, 0, 0, 0, 0, 0, 0 },
		{ 0, 1, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0 }
	};

	//辅助数组
	PathData pathArr[MAP_ROW][MAP_COL];
	for (int i = 0; i < MAP_ROW; ++i)
	{
		for (int j = 0; j < MAP_COL; ++j)
		{
			pathArr[i][j].val = arr[i][j];
			pathArr[i][j].dir = p_up;
			pathArr[i][j].isFind = false;
		}
	}

	MyPoint beginPoint = { 1, 1 };
	MyPoint endPoint = { 5, 7 };
	pathArr[beginPoint.row][beginPoint.col].isFind = true;//起点已经被访问

	//定义一个根节点，用来保存起点，把起点坐标做为根节点的数据域
	MyPathNode *pRoot = new MyPathNode;
	pRoot->pos = beginPoint;
	pRoot->parent = NULL;

	vector<MyPathNode *> nodeList;//正在查找的元素数组
	nodeList.push_back(pRoot);//把根节点压入准备查找的数组

	vector<MyPathNode *> tempNodeList;//下一层待查找的元素数组（当前正在查找的数组的所有的子树）

	MyPoint tempPos;//临时坐标，用来保存当前点的下一个位置，以便判断，下一个位置是否可通行

	while (true)
	{
		bool isEnd = false;//定义一个开关，是否找到终点
		for (int i = 0; i < (int)nodeList.size(); ++i)//循环n次，准备查找的数组中有几个元素就循环多少次
		{
			for (int j = 0; j < 4; ++j)//4方向循环
			{
				tempPos = nodeList[i]->pos;//把准备查找的数组中取出某个元素
				switch (j)
				{
				case p_up:
					tempPos.row--;
					break;
				case p_down:
					tempPos.row++;
					break;
				case p_left:
					tempPos.col--;
					break;
				case p_right:
					tempPos.col++;
					break;
				}
				if (isCheckPoint(tempPos, pathArr))//判断当前点的下一步，是否可通行
				{
					//如果条件满足，证明这个子节点可通行
					MyPathNode *pNode = new MyPathNode;//给一个节点
					pNode->pos = tempPos;//保存这个可通行的子节点

					//关联父子关系
					pNode->parent = nodeList[i];//子节点关联父节点					
					nodeList[i]->pChildNode.push_back(pNode);//父节点关联子节点

					pathArr[tempPos.row][tempPos.col].isFind = true;//这个子节点已经被访问

					tempNodeList.push_back(pNode);//把这个可通行的子节点压入到下一层待查找数组

					//找到终点，树状结构从下往上找很方便
					if (tempPos.row == endPoint.row && tempPos.col == endPoint.col)
					{
						isEnd = true;//表示找到终点
						//打印路径
						MyPathNode * tempNode = pNode;
						while (tempNode)
						{
							printf("row == %d, col == %d\n", tempNode->pos.row, tempNode->pos.col);
							tempNode = tempNode->parent;
						}
						break;//跳出j循环
					}
				}
			}
			if (isEnd)
				break;//找到终点，并跳出i循环
		}
		if (isEnd)
			break;//找到终点，并跳出while循环
		if (tempNodeList.size() == 0) //下一层待查找的数组没有元素，没有找到路径
			break;
		nodeList = tempNodeList;//把下一层待查找的数组赋值给当前正在查找的数组（更新一下正在查找的数组）
		tempNodeList.clear();//把下一层待查找的数组清空
	}

	clearTree(pRoot);
	return 0;
}

