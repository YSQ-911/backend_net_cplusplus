// test07.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "MyStack.h"

//深度寻路  走迷宫的寻路方式
//特点：
//1、一次走一个路口
//2、遇到障碍无法前行，后退一步(后退一个路口)
//3、走过的路不再走第二次

//沿着一个固定的方向进行查找，如果找到终点，结束，如果没有路径，或者不是终点（之前走过），换个方向继续查找

//总结深度的规律：
//1、深度寻路，如果有路，可能会找到一条路，这条路不一定是最短路径
//2、如果有多条路径，只能一条路径
//3、深度寻路的过程取决于方向的制定

//1、深度寻路最简单
//2、在一个开阔的地形，没有障碍，通过方向的制定可以人为制造最短路径

//思考题：深度在有路的情况下不一定能找到路，为什么？


#define MAP_ROW 12
#define MAP_COL 12

//第2个准备 在地图中需要记录走过的方向
enum Path_Dir {p_up,p_down,p_left,p_right};

//第3个准备 在地图中有row,col坐标，来表示路径点
struct MyPoint
{
	int row, col;//路径点的行列值
};

//第4个准备 资源数组是一个int数组，如果有一个路径点被访问，怎么记录，每个路径点的方向需要记录
struct PathNode//准备一个专门用来寻路的地图节点，这个节点可以记录路径点是否被访问，也可以记录当前路径点的方向
{
	int val;//用来表示原始地图的数据
	Path_Dir dir;//标记当前要走的方向
	bool isFind;//标记当前路径点是否已经走过了
};

int _tmain(int argc, _TCHAR* argv[])
{
	//第1步：准备一个地图(资源地图一般不修改)
	int arr[MAP_ROW][MAP_COL] = {
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
		{1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1},
		{1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1},
		{1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 1, 1},
		{1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1},
		{1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1},
		{1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1},
		{1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1},
		{1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1},
		{1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
		{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
	};
	//第5步准备一个辅助的数组，这个辅助数组里面有每个位置的辅助信息
	PathNode pathArr[MAP_ROW][MAP_COL];
	for (int i = 0; i < MAP_ROW; ++i)
	{
		for (int j = 0; j < MAP_COL; ++j)
		{
			pathArr[i][j].val = arr[i][j];//把资源地图的信息，赋值进辅助数组
			pathArr[i][j].isFind = false;//认为辅助数组里面所有的路径点都没有被访问
			pathArr[i][j].dir = p_up;//每一个路径点都先从上方向开始
		}
	}

	//第6步，设定起点和终点
	MyPoint beginPoint = { 1, 1 };
	MyPoint endPoint = { 10, 10 };

	//第7步，准备一个容器用来保存寻路的路径点
	CMyStack<MyPoint> ms;//准备一个容器
	ms.push(beginPoint);

	MyPoint NearPoint = ms.GetTop();//准备一个辅助坐标点，用来辅助当前位置的下一个位置是否可通过

	//第8步开始寻路
	while (true)//不清楚寻找多少次
	{
		switch (pathArr[NearPoint.row][NearPoint.col].dir)//进到这个路口，应该朝哪个方向
		{
		case p_up:
			pathArr[NearPoint.row][NearPoint.col].dir = p_down;//标记当前点的下一个方向
			if (pathArr[NearPoint.row - 1][NearPoint.col].val == 0//证明这个位置上方向位置为0，可以通过，不是障碍
				&& pathArr[NearPoint.row - 1][NearPoint.col].isFind == false)//证明这个位置的上方向位置没有被访问
			{
				//证明可以通行				
				pathArr[NearPoint.row][NearPoint.col].isFind = true;//当前点已经被访问过，防止当前点的下一步检索的时候往回走
				MyPoint tempPos = { NearPoint.row - 1, NearPoint.col };
				ms.push(tempPos);//把可通行的坐标放进容器，以供下次再进行查找
				NearPoint = tempPos;//把这个当前可通行的坐标放到下一次循环检索的值中
			}
			//else
			//	pathArr[NearPoint.row][NearPoint.col].dir = p_down;
			break;
		case p_down:
			pathArr[NearPoint.row][NearPoint.col].dir = p_left;
			if (pathArr[NearPoint.row + 1][NearPoint.col].val == 0
				&& pathArr[NearPoint.row + 1][NearPoint.col].isFind == false)
			{
				pathArr[NearPoint.row][NearPoint.col].isFind = true;
				MyPoint tempPos = { NearPoint.row + 1, NearPoint.col };
				ms.push(tempPos);
				NearPoint = tempPos;
			}
			break;
		case p_left:
			pathArr[NearPoint.row][NearPoint.col].dir = p_right;
			if (pathArr[NearPoint.row][NearPoint.col - 1].val == 0
				&& pathArr[NearPoint.row][NearPoint.col - 1].isFind == false)
			{
				pathArr[NearPoint.row][NearPoint.col].isFind = true;
				MyPoint tempPos = { NearPoint.row, NearPoint.col - 1 };
				ms.push(tempPos);
				NearPoint = tempPos;
			}
			break;
		case p_right:
			//当前这个方向，是我们认定的最后一个方向了
			if (pathArr[NearPoint.row][NearPoint.col + 1].val == 0
				&& pathArr[NearPoint.row][NearPoint.col + 1].isFind == false)
			{
				pathArr[NearPoint.row][NearPoint.col].isFind = true;
				MyPoint tempPos = { NearPoint.row, NearPoint.col + 1 };
				ms.push(tempPos);
				NearPoint = tempPos;
			}
			else
			{
				//最后一个方向就会有else，代表这个路径点四个方向都没有路了
				MyPoint tempPos = ms.GetTop();
				pathArr[tempPos.row][tempPos.col].isFind = true;//容器中最后一个点（无法通行），也表示被访问
				ms.pop();//回退一步
				if (!ms.empty())//如果容器非空
					NearPoint = ms.GetTop();//把新的栈顶，重新检索
			}
			break;
		}
		if (NearPoint.row == endPoint.row && NearPoint.col == endPoint.col)
			break;//找到路径，结束
		if (ms.empty())//容器为空
			break;//没有路径
	}

	while (!ms.empty())
	{
		MyPoint temp = ms.GetTop();
		printf("row = %d,col = %d\n",temp.row,temp.col);
		ms.pop();
	}

	return 0;
}

