#pragma once

template <class T>
class CMyStack
{
	T *pbuff;
	size_t len;
	size_t maxSize;
public:
	CMyStack();
	~CMyStack();
public:
	void clear();
	void push(T const& elem);
	void pop();
	T const& GetTop() const { return pbuff[len - 1]; }
	bool empty() const { return len == 0; }
};

template <class T>
void CMyStack<T>::pop()
{
	len--;
}

template <class T>
void CMyStack<T>::push(T const& elem)
{
	if (len >= maxSize)
	{
		maxSize = maxSize + ((maxSize >> 1) > 1 ? (maxSize >> 1) : 1);
		T *tempBuff = new T[maxSize];
		memcpy(tempBuff, pbuff, sizeof(T)* len);
		if (pbuff)
			delete[]pbuff;
		pbuff = tempBuff;
	}
	pbuff[len++] = elem;
}

template <class T>
void CMyStack<T>::clear()
{
	if (pbuff)
		delete[]pbuff;
	len = 0;
	pbuff = NULL;
	maxSize = 0;
}

template <class T>
CMyStack<T>::~CMyStack()
{
	clear();
}

template <class T>
CMyStack<T>::CMyStack()
{
	pbuff = NULL;
	len = 0;
	maxSize = 0;
}
