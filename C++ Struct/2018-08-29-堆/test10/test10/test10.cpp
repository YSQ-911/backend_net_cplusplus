// test10.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"

//哈夫曼树  最优二叉树，是一类带权路径长度最短的树

//堆  最大（小）完全二叉树
//最大堆，堆中每个节点的值都大于或等于其任何一个子节点的值

//堆排序什么时候用，如果在项目中不需要用堆来保存数据，不需要堆排序
#include "MyHeap.h"
int _tmain(int argc, _TCHAR* argv[])
{
	//CMyHeap<int> mh;
	//for (int i = 0; i < 10; ++i)
	//	mh.appendNode(i + 1);
	////mh.printfHeap();
	//for (int i = 0; i < 10; ++i)
	//{
	//	printf("%d  ",mh.GetRoot());
	//	mh.deleteHeap();
	//}
	//printf("\n//===================\n");

	int arr[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	CMyHeap<int> mh;
	mh.initHeap(arr, 10);
	mh.printfHeap();
	return 0;
}

