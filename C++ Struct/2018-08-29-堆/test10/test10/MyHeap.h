#pragma once

template<class T>
class CMyHeap
{
	T *pBuff;
	size_t len;
	size_t maxSize;
public:
	CMyHeap();
	~CMyHeap();
	void clear();//清除堆中的所有元素
	void appendNode(T const& srcData);//堆，插入元素
	void deleteHeap();//一次删除一个元素
	void initHeap(T arr[], size_t srcLen);//初始化一个堆
public:
	void printfHeap();
	T const& GetRoot() const;//得到根节点
};

//注意：不能从根节点直接往下比，因为初始化之后，根节点的左右子树不一定是最大堆，所以需要从最后一个有子节点的
//节点开始调整位置，依次往根节点调整，直到最后根节点，这样每一次调整的根节点，它所有的子节点必然是最大堆，才有
//意义
//初始化：1、一次加入堆中所有元素；2、从最后一个有子节点的节点开始，调整位置，满足堆的特性
template<class T>
void CMyHeap<T>::initHeap(T arr[], size_t srcLen)
{
	clear();
	if (srcLen == 0)//如果传进来的资源数组为空，这个函数不执行
		return;
	maxSize = len = srcLen;
	pBuff = new T[maxSize];
	memmove(pBuff, arr, sizeof(T)* len);

	//len代表元素个数，最后元素的下标是len-1,最后一个元素的下标 -1 整除2，就得到最后一个有子节点的元素下标 
	for (int i = (((int)len - 2) >> 1); i >= 0; --i)//最后一个有子节点的节点下标为i
	{
		int index = i;//当前不满足条件的下标，是根节点，为0
		T tempNum = pBuff[index];//保存待插入元素值
		while (true)//不清楚要交换多少次，死循环
		{
			int left = 2 * index + 1;//左子树下标
			int right = 2 * index + 2;//右子树下标
			if (left > (int)len - 1)//没有左子树，左子树的下标越界，也必然没有右子树，当前节点为叶节点，循环结束
				break;
			bool isLeft = true;//默认与左子树比较
			if (right <= (int)len - 1)//有右子树
			{
				if (pBuff[left] < pBuff[right])//有右子树的情况且左子树小于右子树的值，改写isleft的值
					isLeft = false;
			}
			if (isLeft)
			{
				//与左子树比较，左子树比右子树大
				if (tempNum < pBuff[left])
				{
					pBuff[index] = pBuff[left];//把左子树的值赋值到父节点位置
					index = left;//把左子树当成新的父节点
				}
				else
					break;//父节点比最大的左子树还要大，结束调整
			}
			else
			{
				//与右子树比较，右子树比左子树大
				if (tempNum < pBuff[right])
				{
					pBuff[index] = pBuff[right];//把右子树的值赋值到父节点位置
					index = right;//把右子树当成新的父节点
				}
				else
					break;//父节点比最大的左子树还要大，结束调整
			}
		}
		pBuff[index] = tempNum;
	}

}

template<class T>
T const& CMyHeap<T>::GetRoot() const
{
	return pBuff[0];
}

//规则：1、每次先删除根节点；2、把最后一个节点移动到根节点；3、调整位置满足堆的特点
template<class T>
void CMyHeap<T>::deleteHeap()
{
	if (len == 0)//空堆，不需要删除
		return;

	if (len > 1)//如果堆中只有根节点，那if条件len=1，里面的代码不需要执行，没有意义
		pBuff[0] = pBuff[len - 1];
	len--;//删除，如果只有根节点，--后变成空堆，如果有2个，--后变成只有一个根节点的堆，只有2个以上，才需要调整位置

	int index = 0;//当前不满足条件的下标，是根节点，为0
	T tempNum = pBuff[index];//保存待插入元素值
	while (true)//不清楚要交换多少次，死循环
	{
		int left = 2 * index + 1;//左子树下标
		int right = 2 * index + 2;//右子树下标
		if (left > (int)len - 1)//没有左子树，左子树的下标越界，也必然没有右子树，当前节点为叶节点，循环结束
			break;
		bool isLeft = true;//默认与左子树比较
		if (right <= (int)len - 1)//有右子树
		{
			if (pBuff[left] < pBuff[right])//有右子树的情况且左子树小于右子树的值，改写isleft的值
				isLeft = false;
		}
		if (isLeft)
		{
			//与左子树比较，左子树比右子树大
			if (tempNum < pBuff[left])
			{
				pBuff[index] = pBuff[left];//把左子树的值赋值到父节点位置
				index = left;//把左子树当成新的父节点
			}
			else
				break;//父节点比最大的左子树还要大，结束调整
		}
		else
		{
			//与右子树比较，右子树比左子树大
			if (tempNum < pBuff[right])
			{
				pBuff[index] = pBuff[right];//把右子树的值赋值到父节点位置
				index = right;//把右子树当成新的父节点
			}
			else
				break;//父节点比最大的左子树还要大，结束调整
		}
	}
	pBuff[index] = tempNum;
}

template<class T>
void CMyHeap<T>::printfHeap()
{
	for (size_t i = 0; i < len; ++i)
		printf("%d  ",pBuff[i]);
	printf("\n//=====================\n");
}

template<class T>
void CMyHeap<T>::appendNode(T const& srcData)
{
	if (len >= maxSize)
	{
		maxSize = maxSize + ((maxSize >> 1) > 1 ? (maxSize >> 1) : 1);
		T *tempBuff = new T[maxSize];
		memmove(tempBuff, pBuff, sizeof(T)* len);
		if (pBuff)
			delete[]pBuff;
		pBuff = tempBuff;
	}
	pBuff[len++] = srcData;
	//上面代码完成一个完全二叉树的操作

	//不清楚srcData的值，也不确定是否会破坏最大堆规律，所以从当前节点沿纵向次序往上进行判断，如果破坏规律，交换
	int tempIndex = len - 1;//当前节点的下标
	T tempNum = pBuff[tempIndex];//按插入排序的规律，把当前元素另行保存
	while (tempIndex)
	{
		int parentIndex = (tempIndex - 1) >> 1;//求父节点的下标，当前节点需要和父节点进行比较
		if (tempNum > pBuff[parentIndex])
		{
			pBuff[tempIndex] = pBuff[parentIndex];//把父节点下标表示的值，赋值给当前下标表示的位置
		}
		else
			break;
		tempIndex = parentIndex;
	}
	pBuff[tempIndex] = tempNum;
}

template<class T>
void CMyHeap<T>::clear()
{
	if (pBuff)
		delete[] pBuff;
	pBuff = NULL;
	len = maxSize = 0;
}

template<class T>
CMyHeap<T>::~CMyHeap()
{
	clear();
}

template<class T>
CMyHeap<T>::CMyHeap()
{
	pBuff = NULL;
	len = maxSize = 0;
}
