#pragma once

template <class T>
class CMyTree_Arr
{
	T *pBuff;
	size_t len;
	size_t maxSize;
public:
	CMyTree_Arr();
	~CMyTree_Arr();
	void clear();
	bool find(T const& findVal) const;
	void appendNode(T const& data);
	void initTree(T arr[], size_t length);
	T const& GetParentVal(T const& findVal) const;
	T const& GetLeftChildVal(T const& findVal) const;
	T const& GetRightChildVal(T const& findVal) const;
	T const& GetLeftBrotherVal(T const& findVal) const;
	T const& GetRightBrotherVal(T const& findVal) const;
	void printfTree();//遍历树
private:
	int _find(T const& findVal) const;
	void _printfTree(int index);
};

template <class T>
void CMyTree_Arr<T>::_printfTree(int index)
{
	if (index < (int)len)
	{
		
		_printfTree(2 * index + 1);
		
		_printfTree(2 * index + 2);
		printf("%d\n", pBuff[index]);//后根
	}
}

template <class T>
void CMyTree_Arr<T>::printfTree()
{
	if (len)
		_printfTree(0);
	else
		printf("空树\n");
}

template <class T>
T const& CMyTree_Arr<T>::GetRightBrotherVal(T const& findVal) const
{
	int index = _find(findVal);
	//index <= 0 <0表示没有找到这个节点，树中无此节点，所以没有右兄弟，==0表示找到根节点，也没有右兄弟
	//index % 2 == 2  表示这个节点就是右子树，所以也没有右兄弟
	//index + 1 >= len 表示越界了
	if (index <= 0 || index % 2 == 0 || index + 1 >= len)
		throw "no right brother";
	return pBuff[index + 1];
}

template <class T>
T const& CMyTree_Arr<T>::GetLeftBrotherVal(T const& findVal) const
{
	int index = _find(findVal);
	//index <= 0 <0表示没有找到这个节点，树中无此节点，所以没有左兄弟，==0表示找到根节点，也没有左兄弟
	//index % 2 == 1  表示这个节点就是左子树，所以也没有左兄弟
	if (index <= 0 || index % 2 == 1)
		throw "no left brother";
	return pBuff[index-1];
}

template <class T>
T const& CMyTree_Arr<T>::GetRightChildVal(T const& findVal) const
{
	int index = _find(findVal);
	//index < 0 表示没有找到这个节点，树中无此节点，所以没有右子树
	//2 * index + 2 >= len  表示这个节点越界位置
	if (index < 0 || 2 * index + 2 >= len)
		throw "no right child";
	return pBuff[2 * index + 2];
}

template <class T>
T const& CMyTree_Arr<T>::GetLeftChildVal(T const& findVal) const
{
	int index = _find(findVal);
	//index < 0 表示没有找到这个节点，树中无此节点，所以没有左子树
	//2 * index + 1 >= len  表示这个节点越界位置
	if (index < 0 || 2 * index + 1 >= len)
		throw "no left child";
	return pBuff[2 * index + 1];
}

template <class T>
T const& CMyTree_Arr<T>::GetParentVal(T const& findVal) const
{
	int index = _find(findVal);
	if (index <= 0)//如果_find函数没有找到findVal这个值，返回-1，如果找到的是根节点，返回0
		throw "no parent";
	return pBuff[(index - 1) >> 1];
}

template <class T>
void CMyTree_Arr<T>::initTree(T arr[], size_t length)
{
	clear();
	maxSize = len = length;
	pBuff = new T[maxSize];
	memcpy(pBuff, arr, sizeof(T)* len);
}

template <class T>
void CMyTree_Arr<T>::appendNode(T const& data)
{
	if (len >= maxSize)
	{
		maxSize = maxSize + ((maxSize >> 1) > 1 ? (maxSize >> 1) : 1);
		T *tempBuff = new T[maxSize];
		memcpy(tempBuff, pBuff, sizeof(T)* len);
		if (pBuff)
			delete[]pBuff;
		pBuff = tempBuff;
	}
	pBuff[len++] = data;
}

template <class T>
int CMyTree_Arr<T>::_find(T const& findVal) const
{
	for (size_t i = 0; i < len; ++i)
	{
		if (pBuff[i] == findVal)
			return i;
	}
	return -1;
}

template <class T>
bool CMyTree_Arr<T>::find(T const& findVal) const
{
	return _find(findVal) != -1;
}

template <class T>
void CMyTree_Arr<T>::clear()
{
	if (pBuff)
		delete[] pBuff;
	pBuff = NULL;
	len = maxSize = 0;
}

template <class T>
CMyTree_Arr<T>::~CMyTree_Arr()
{
	clear();
}

template <class T>
CMyTree_Arr<T>::CMyTree_Arr()
{
	pBuff = NULL;
	len = maxSize = 0;
}
