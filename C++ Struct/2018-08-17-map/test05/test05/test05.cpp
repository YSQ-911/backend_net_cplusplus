// test05.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"


void aaa(int arr[], int len)
{
	int tempVal;
	for (int i = 0; i < len - 1; ++i)
	{
		for (int j = i + 1; j < len; ++j)
		{
			if (arr[i] > arr[j])
			{
				tempVal = arr[i];
				arr[i] = arr[j];
				arr[j] = tempVal;
			}
		}
	}
}

//map  关系式容器（已序）   平衡二叉树
//把元素分成key，value，把key和value当成一个组来进行管理，默认已升序来进行排列，通过key的值来进行排序
//1、key和value必须具备可复制和可赋值的性质
//2、key必须是可以比较的
//3、map是已序的，通过key来进行排序，意味着通过key来查找效率很高，反之通过value来查找，效率很低

//所写的代码，就是分析代码所涉及的数据，及合理的组合数据，并且规则解决问题的算法的过程
//算法：解决问题的具体流程
//对于算法而言：空间复杂度，时间复杂度
//空间复杂度：指令空间，数据空间，环境栈空间
//时间复杂度：这个算法运行完后需要的时间

#include <vector>
using std::vector;

#include <map>
using std::map;
#include <xfunctional>

using std::multimap;//也在map文件里面

#include <set>
using std::set;//key和val是同一个


int _tmain(int argc, _TCHAR* argv[])
{
	vector<int> v;
	v.push_back(1);



	//允许有重复的key，不会出现覆盖的现象
	multimap<int, double> mm;
	mm.insert(std::make_pair(4, 4.1));
	mm.insert(std::make_pair(1, 0.3));
	mm.insert(std::make_pair(1, 4.1));

	multimap<int, double>::iterator mmit;
	for (mmit = mm.begin(); mmit != mm.end(); ++mmit)
		printf("key = %d,val = %f\n",mmit->first,mmit->second);
	printf("//==============\n");



	//key只能是唯一，如果有多个相同的key，后面的key所映射的val会覆盖掉前面的val
	map<int, double> m;//不是数组，不是线性结构
	m[1] = 1.97;//key为1,value为1.97，把1和1.97建立映射关系 
	m[5] = 0.3;
	m[2] = 9.3;
	m[0] = 3.14;
	m[1] = 7.7;

	map<int, double>::iterator mit;
	for (mit = m.begin(); mit != m.end(); ++mit)
		printf("key = %d,value = %f\n",mit->first,(*mit).second);//first表示迭代器所指的key值，second表示迭代器位置key所映射的value

	//std::greater 是std里面的一个结构，和map没有直接的关系
	map<float, std::string, std::greater<float>> mf;//key为float,val为string，排序准则不用默认，用降序
	mf[1.3f] = "zhang san";
	mf[0.2f] = "li si";
	mf[4.5f] = "wang wu";

	//1、通过组的方式来进行插入  pair(组)，是std里面的一个结构，和map没有直接的关系，类似于std::greater
	mf.insert(std::pair<float, std::string>(2.4f, "abc"));
	//2、通过std里面的一个函数来进行插入
	mf.insert(std::make_pair(9.8f, "123"));
	//3、通过map里面的一个结构来进行插入
	mf.insert(map<float, std::string, std::greater<float>>::value_type(4.3f, ";'."));

	mf.erase(1.3f);//删除通过key来进行删除 

	map<float, std::string, std::greater<float>>::iterator mfit;
	for (mfit = mf.begin(); mfit != mf.end(); ++mfit)
		printf("key = %f,value = %s\n", mfit->first, (*mfit).second.c_str());//first表示迭代器所指的key值，second表示迭代器位置key所映射的value
	
	mfit = mf.find(0.2f);
	//mfit = mf.find("li si");错误，只能通过key查找
	printf("//==============\n");
	printf("key = %f,value = %s\n", mfit->first, (*mfit).second.c_str());
	
	return 0;
}

