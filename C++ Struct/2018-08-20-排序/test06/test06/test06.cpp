// test06.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <windows.h>
#pragma comment (lib,"Winmm.lib")

#define ARR_LEN 40000

void printfArr(int arr[], int len)
{
	for (int i = 0; i < len; ++i)
		printf("%d  ",arr[i]);
	printf("\n");
}

//排序
void Select_Sort(int arr[], int len)//选择,一次选择一个位置，直到这个位置上的数据完全满足条件才换下一个位置
{
	int tempVal;
	for (int i = 0; i < len - 1; ++i)
	{
		for (int j = i + 1; j < len; ++j)
		{
			if (arr[i] > arr[j])
			{
				tempVal = arr[i];
				arr[i] = arr[j];
				arr[j] = tempVal;
			}
		}
	}
}

void Buble_Sort(int arr[], int len)//冒泡,两个相邻的位置来依次比较，满足条件的数据会依次冒到最后面，再从头开始
{
	int tempVal;
	for (int i = 0; i < len - 1; ++i)
	{
		for (int j = 0; j < len - 1 - i; ++j)
		{
			if (arr[j] > arr[j + 1])
			{
				tempVal = arr[j + 1];
				arr[j + 1] = arr[j];
				arr[j] = tempVal;
			}
		}
	}
}

void Insert_Sort(int arr[], int len)//插入  最坏的可能性和选择（冒泡）最坏的可能性一致，最好的可能性，每一次只比较一次
{
	int tempVal;
	int j;
	for (int i = 1; i < len; ++i)
	{
		tempVal = arr[i];
		j = i - 1;
		while (j >= 0 && tempVal < arr[j])
		{
			arr[j + 1] = arr[j];
			--j;
		}
		arr[j + 1] = tempVal;
	}

	//int tempVal;
	//int j;
	//for (int i = 1; i < len; ++i)
	//{
	//	tempVal = arr[i];
	//	for (j = i - 1; j >= 0; --j)
	//	{
	//		if (tempVal < arr[j])
	//		{
	//			arr[j + 1] = arr[j];
	//		}
	//		else
	//			break;
	//	}
	//	arr[j + 1] = tempVal;
	//}

	//int tempVal;
	//for (int i = 1; i < len; ++i)
	//{
	//	for (int j = i - 1; j >= 0; --j)
	//	{
	//		if (arr[j + 1] < arr[j])
	//		{
	//			tempVal = arr[j + 1];
	//			arr[j + 1] = arr[j];
	//			arr[j] = tempVal;
	//		}
	//		else
	//			break;
	//	}
	//}
}

void Shell_Sort(int arr[], int len)//希尔，基于插入排序
{
	int tempVal;
	int j;
	int jump = len >> 1;//步长值
	while (jump != 0)
	{
		for (int i = jump; i < len; ++i)
		{
			tempVal = arr[i];
			j = i - jump;
			while (j >= 0 && tempVal < arr[j])
			{
				arr[j + jump] = arr[j];
				j -= jump;
			}
			arr[j + jump] = tempVal;
		}
		jump = jump >> 1;
	}
}

//分而治之的排序法
//归并
void _merge_in_arr(int arr[], int left, int mid, int right)
{
	int length = right - left + 1;
	int *pData = new int[length];//准备一个辅助数组，长度为归过来的元素个数
	memset(pData, 0, sizeof(int)* length);//数组初始为0

	int low = left;//定义一个变量，为左边的下标，防止改为left的值
	int hig = mid + 1;
	int index = 0;//辅助数组的下标

	while (hig <= right)//右边没有合并完成
	{
		//如果左边的值小于等于右边的值，且左边没有越界
		while (arr[low] <= arr[hig] && low <= mid)
		{
			pData[index] = arr[low];//把左边的值放进辅助数组
			low++;//左边的下标往右++;
			index++;
		}
		if (low > mid)//证明左边放完了，左边的数据没有了
			break;
		//如果左边的值大于右边的值，且右边没有越界
		while (arr[low] > arr[hig] && hig <= right)
		{
			pData[index] = arr[hig];//把右边的值放进辅助数组
			hig++;//右边的下标往右++;
			index++;
		}
	}

	//到这一步，证明起码有一边是放完了
	if (low <= mid)//证明左边没有放完
		memmove(&pData[index], &arr[low], sizeof(int)* (mid - low + 1));
	if (hig <= right)//证明右边没有放完
		memmove(&pData[index], &arr[hig], sizeof(int)* (right - hig + 1));

	//到这一步，把有数据都按次序放在辅助数组里面

	memmove(&arr[left], pData, sizeof(int)* length);
	delete[]pData;
}

void _merge(int arr[], int left, int right)
{
	if (left >= right)
		return;

	int mid = left + ((right - left) >> 1);
	_merge(arr, left, mid);//递归左区间
	_merge(arr, mid + 1, right);//递归右区间
	//并操作
	_merge_in_arr(arr, left, mid, right); 
}

void Merge_Sort(int arr[], int len)
{
	_merge(arr, 0, len - 1);
}

//基数排序（桶排序）
void Radix_Sort(int arr[], int len)
{
	for (int n = 1; n <= 10000; n *= 10)//代表个，十，百位数
	{
		int temp[10][10] = {};//定义辅助数组
		for (int x = 0; x < 10; ++x)//辅助数组初始化
		{
			for (int y = 0; y < 10; ++y)
				temp[x][y] = -1;
		}
		//待排序的数据放进辅助数组
		for (int i = 0; i < len; ++i)
		{
			int index = (arr[i] / n) % 10;
			temp[index][i] = arr[i];
		}
		//把辅助数组的数据重新放回数组中
		int k = 0;
		for (int i = 0; i < 10; ++i)
		{
			for (int j = 0; j < 10; ++j)
			{
				if (temp[i][j] != -1)
					arr[k++] = temp[i][j];
			}
		}
	}
}

int _tmain(int argc, _TCHAR* argv[])
{
	//timeGetTime 可以得到毫秒
	srand(timeGetTime());

	int arr[ARR_LEN] = {};
	int arr1[ARR_LEN] = {};
	int arr2[ARR_LEN] = {};
	int arr3[ARR_LEN] = {};
	int arr4[ARR_LEN] = {};
	int arr5[ARR_LEN] = {};
	for (int i = 0; i < ARR_LEN; ++i)
	{
		arr[i] = rand() % 1000;
		arr1[i] = arr[i];
		arr2[i] = arr[i];
		arr3[i] = arr[i];
		arr4[i] = arr[i];
		arr5[i] = arr[i];
	}
	//printfArr(arr, ARR_LEN);

	float beginTimer = timeGetTime() / 1000.0f;
	Select_Sort(arr, ARR_LEN);
	float endTimer = timeGetTime() / 1000.0f;
	printf("select : %f\n",endTimer - beginTimer);
	//printfArr(arr, ARR_LEN);

	beginTimer = timeGetTime() / 1000.0f;
	Buble_Sort(arr1, ARR_LEN);
	endTimer = timeGetTime() / 1000.0f;
	printf("bubble : %f\n", endTimer - beginTimer);
	//printfArr(arr1, ARR_LEN);

	beginTimer = timeGetTime() / 1000.0f;
	Insert_Sort(arr2, ARR_LEN);
	endTimer = timeGetTime() / 1000.0f;
	printf("insert : %f\n", endTimer - beginTimer);
	//printfArr(arr2, ARR_LEN);

	beginTimer = timeGetTime() / 1000.0f;
	Shell_Sort(arr3, ARR_LEN);
	endTimer = timeGetTime() / 1000.0f;
	printf("shell : %f\n", endTimer - beginTimer);
	//printfArr(arr3, ARR_LEN);

	beginTimer = timeGetTime() / 1000.0f;
	Merge_Sort(arr4, ARR_LEN);
	endTimer = timeGetTime() / 1000.0f;
	printf("merge : %f\n", endTimer - beginTimer);
	//printfArr(arr4, ARR_LEN);

	//Radix_Sort(arr5, ARR_LEN);
	//printfArr(arr5, ARR_LEN);

	return 0;
}

