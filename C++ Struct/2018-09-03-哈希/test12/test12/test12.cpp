// test12.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "string.h"
#include "vector"
using std::vector; 

//哈希表
//哈希法 又名散列法，是一种特殊的查找方法
//哈希法就是希望不通过比较，一次存取就可以得到所查的元素
//哈希表把数据的存放地址定义为记录关键字的函数
//这个函数值也叫哈希值
//哈希法的平均比较次数和表中所含的节点个数无关，这种方法既是一种查找方法，也是一种确定数据存储地址的方法
//冲突现象
//哈希表中实际存入的元素个数 / 哈希表中的最大容量
//处理冲突现象的方法有两种，一种是放在后续相临位置，另一种用链式结构

//设计一个哈希表
//1、确定表的空间范围，确定哈希函数值域
//2、构造合适的哈希函数，该函数要保证所有的元素的哈希值都在指定的值域内
//3、选择处理冲突的方法

//定义一个好的哈希函数是关键
//1、自身函数
//2、数字分析法

//中心思想只有一个，分类，把一个数据设计一个可分类的id号，通过id的每一个位来进行求余，得到一个相对位置，再通过下个小的分类，做相同的操作

struct MyNode//处理冲突(同义词)
{
	int data;
	MyNode *pNext;
};

//struct MyTreeNode
//{
//	int data;
//	MyTreeNode * parent;
//	vector<MyTreeNode *> pchild;
//};

struct _Hash_Table//哈希表
{
	MyNode *value[10];//指针数组，这个指针数组里面的元素是指针，又可以指向一个链表
};

int getIndex(int val)
{
	return val % 10;
}

_Hash_Table * create_hash_table()//创建哈希表
{
	_Hash_Table *pHash = new _Hash_Table;
	memset(pHash, 0, sizeof(_Hash_Table));
	return pHash;
}

//插入元素进入到哈希表
bool insert_data_into_hash(_Hash_Table *pHashTable, int data)
{
	if (pHashTable == nullptr)//哈希表不存在，不能插入
		return false;

	MyNode *pNode;//准备一个节点

	if ((pNode = pHashTable->value[data % 10]) == nullptr)//证明当前的下标位置
	//if ((pNode = pHashTable->value[getIndex(data)]) == nullptr)//证明当前的下标位置这个链表不存在
	{
		pNode = new MyNode;
		pNode->data = data;
		pNode->pNext = nullptr;
		pHashTable->value[data % 10] = pNode;
		return true;
	}
	else
	{
		//证明当前下标的位置有链表存在，意味着哈希表有冲突
		//pNode = pHashTable->value[data % 10];
		while (pNode->pNext)
			pNode = pNode->pNext;
		pNode->pNext = new MyNode;
		pNode->pNext->data = data;
		pNode->pNext->pNext = nullptr;
		return true;
	}
	return false;
}

//查找元素
MyNode * find_data_in_hash(_Hash_Table * pHashTable, int findData)
{
	if (pHashTable == nullptr)
		return nullptr;

	MyNode *pNode;
	if ((pNode = pHashTable->value[findData % 10]) == nullptr)
		return nullptr;
	while (pNode)//代表着链表存在，起码有这个同义词
	{
		if (pNode->data == findData)
			return pNode;
		pNode = pNode->pNext;
	}
	return nullptr;
}

bool delete_data_from_hash(_Hash_Table *pHashTable, int delData)
{
	MyNode *pNode;
	if ((pNode = find_data_in_hash(pHashTable, delData)) == nullptr)
		return false;
	MyNode *pHead;
	//判断待删除是不是这个位置链表的头节点
	if ((pHead = pHashTable->value[delData % 10]) == pNode)
	{
		//条件满足证明待删除的就是链表头节点
		pHashTable->value[delData % 10] = pNode->pNext;
	}
	else
	{
		//条件满足证明待删除的不是链表头节点
		while (pHead->pNext != pNode)
			pHead = pHead->pNext;
		pHead->pNext = pNode->pNext;
	}

	delete pNode;
	return true;
}

void clear_hash_table(_Hash_Table *& pHashTable)
{
	if (pHashTable == nullptr)
		return;

	MyNode *pHead;
	MyNode *pTemp;
	//循环删除指针数组里面的每一个指向链表的指针
	for (int i = 0; i < 10; ++i)
	{
		//if (pHead = pHashTable->value[i])
		if ((pHead = pHashTable->value[i]) != nullptr)
		{
			while (pHead)
			{
				pTemp = pHead;
				pHead = pHead->pNext;
				delete pTemp;
			}
		}
	}
	delete pHashTable;
	pHashTable = nullptr;
}

int _tmain(int argc, _TCHAR* argv[])
{
	_Hash_Table *pHash = nullptr;
	pHash = create_hash_table();//得到了一个哈希表

	insert_data_into_hash(pHash, 1);
	insert_data_into_hash(pHash, 11);
	insert_data_into_hash(pHash, 111);

	delete_data_from_hash(pHash, 11);

	if (find_data_in_hash(pHash, 11))
		printf("找到了11\n");

	if (find_data_in_hash(pHash, 111))
		printf("找到了111\n");

	clear_hash_table(pHash);

	return 0;
}

