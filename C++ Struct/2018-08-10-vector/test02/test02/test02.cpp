// test02.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <vector>
using std::vector;

//vector  序列式容器  可序
//1、来模拟vector的代码

//vector  表现的是一个动态数组 （并没有要求必须以动态数组来实现vector）
//1、随机的存取，只要知道位置，可以在一个常数时间内存取这个位置上的元素
//2、在末尾进行插入和删除，性能好
//3、前端和中部进行插入和删除，性能差
//4、内存的重新分配，来保证动态变化 ，但是内存的重分配，会导致效率的降低

int _tmain(int argc, _TCHAR* argv[])
{
	vector<int> value(2,3);
	vector<int> v1;
	v1 != value;
	vector<char> v(4, 'a');
	if (!v.empty())
		v.front();
	v.reserve(4);
	v.resize(10);
	return 0;
}

