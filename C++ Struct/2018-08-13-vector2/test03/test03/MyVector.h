#pragma  once

//作业：提交到我的企业QQ邮箱 3002715991
//		提交的格式：提交截图(截图)及代码（截图）
//		提交的时间：下一次上课的下午3点前
//		命名：**期**学号**网名**作业

//namespace MyStd
//{
//
//}
template<typename T>
class CMyVector
{
	T			*pBuff;//动态数组的首地址
	size_t		iLength;//动态数组里面存放在元素个数
	size_t		iMaxSize;//动态数组的最大长度
public:
	struct MyIterator
	{
		T *pIt;
		MyIterator & operator = (MyIterator const& srcIt)
		{
			pIt = srcIt.pIt;
			return *this;
		}
		bool operator != (MyIterator const& srcIt) const
		{
			return pIt != srcIt.pIt;
		}
		MyIterator& operator++()
		{
			pIt++;
			return *this;
		}
		MyIterator operator++(int)
		{
			MyIterator tempIt = *this;
			pIt++;
			return tempIt;
		}
		T operator *()
		{
			return *pIt;
		}
		MyIterator operator+(int n)
		{
			MyIterator tempIt = *this;
			tempIt.pIt += n;
			return tempIt;
		}
		int operator - (MyIterator const& srcIt)
		{
			return pIt - srcIt.pIt;
		}
	};
public:
	CMyVector();
	CMyVector(CMyVector const& other);
	CMyVector(int n);//生成一个容器，这个容器里面有n个数据，每一个数据利用T这个类型的默认构造
	CMyVector(int n, T const& elem);//生成一个容器，里面有n个数据，每一个数据是elem的备份
	CMyVector(MyIterator const& first, MyIterator const& second);
	~CMyVector();
	void clear();
public:
	//在pos的位置插入一个区间，first是区间的头，second是区间的尾
	MyIterator insert(MyIterator const& pos, MyIterator const& first, MyIterator const& second)
	{
		MyIterator tempIt = pos;
		int n = (MyIterator)second - (MyIterator)first;
		for (int i = 0; i < n; ++i)
			tempIt = insert(tempIt, *(first + i));
		return tempIt;
	}
	MyIterator insert(MyIterator const& pos, int n, T const& elem)//在pos位置插入n个elem
	{
		MyIterator tempIt = pos;
		for (int i = 0; i < n; ++i)
			tempIt = insert(tempIt, elem);
		return tempIt;
	}
	MyIterator insert(MyIterator const& pos, T const& elem)//在pos位置处插入elem，并返回新的元素位置
	{
		int num = pos.pIt - pBuff;
		if (iLength >= iMaxSize)
		{
			iMaxSize = iMaxSize + ((iMaxSize >> 1) > 1 ? (iMaxSize >> 1) : 1);
			T *tempBuff = new T[iMaxSize];
			memcpy(tempBuff, pBuff, sizeof(T)* iLength);
			if (pBuff)
				delete[] pBuff;
			pBuff = tempBuff;
		}
		for (int i = iLength; i > num; --i)
			pBuff[i] = pBuff[i - 1];
		pBuff[num] = elem;
		iLength++;
		MyIterator tempIt;
		tempIt.pIt = pBuff + num;
		return tempIt;
	}
	MyIterator erase(MyIterator const &pos)//系统的删除，可能重分配空间
	{
		int num = pos - pBuff;
		for (int i = num; i < iLength - 1; ++i)
			pBuff[i] = pBuff[i + 1];
		iLength--;
		MyIterator tempIt;
		tempIt.pIt = pBuff + num;
		return tempIt;
	}
	MyIterator erase(MyIterator const& first, MyIterator const& second)
	{
		int n = (MyIterator)second - (MyIterator)first;
		MyIterator tempIt = (MyIterator)first;
		for (int i = 0; i < n; ++i)
			tempIt = erase(tempIt);
		return tempIt;
	}
public:
	MyIterator begin()
	{
		MyIterator tempIt;
		tempIt.pIt = pBuff;
		return tempIt;
	}
	MyIterator end()
	{
		MyIterator tempIt;
		tempIt.pIt = pBuff + iLength;
		return tempIt;
	}
public:
	size_t size() const;//返回当前元素的数量
	bool empty() const;//判断容器是否为空
	size_t capacity() const;//返回当前容器的最大容纳元素的数量
	void reserve(int n);//如果容量不足，扩大容量
public:
	bool operator==(CMyVector const& srcVector) const;//运算符重载，判断容器是否相等
	bool operator != (CMyVector const& srcVector) const;//运算符重载，判断容器是否不相等
	bool operator > (CMyVector const& srcVector) const;//作业
	bool operator >= (CMyVector const& srcVector) const;//作业
	bool operator < (CMyVector const& srcVector) const;//作业
	bool operator <=(CMyVector const& srcVector) const;//作业
public:
	//作业：赋值运算符重载
	CMyVector& operator=(CMyVector & srcVector)
	{
		clear();
		this->iLength = srcVector.iLength;
		iMaxSize = srcVector.iMaxSize;
		if (iMaxSize != 0)
		{
			pBuff = new T[iMaxSize];
			memcpy(pBuff, srcVector.pBuff, sizeof(T)* iLength);
		}
	}
	void assign(int n, T const& elem);//复制n个elem，赋值给this
	void assign(MyIterator const& first, MyIterator const& second);
	void swap(CMyVector & srcVector);//交换，两个容器交换数据
	//void swap(CMyVector& v1, CMyVector &v2);
	//作业：交换，但需要有两个参数
	friend void swap(CMyVector & v1, CMyVector & v2)
	{
		CMyVector tempV = v1;
		v1 = v2;
		v2 = tempV;
	}
public:
	T & at(int index) const;//返回下标index位置上的元素(注意：at是如果index越界，会抛出异常)
	T & operator[](int index) const;//返回下标index位置上的元素
	T & front();//返回第一个元素，并不检查是否存在
	T & back();//返回最后一个元素，并不检查是否存在
public:
	void push_back(T const& elem);//尾部添加数据
	void pop_back();//尾部删除
	void resize(int n);//将元素个数改为n,如果size变大了，多出来的用默认构造去创建，size变小了，直接改len
	void resize(int n, const T & elem);
};

template<typename T>
void CMyVector<T>::assign(MyIterator const& first, MyIterator const& second)
{
	clear();
	int n = (MyIterator)second - (MyIterator)first;
	iLength = iMaxSize = n;
	if (iLength > 0)
	{
		pBuff = new T[iMaxSize];
		for (int i = 0; i < n; ++i)
			pBuff[i] = *(first + i);
	}
}

template<typename T>
CMyVector<T>::CMyVector(MyIterator const& first, MyIterator const& second)
{
	int n = (MyIterator)second - (MyIterator)first;
	iLength = iMaxSize = n;
	pBuff = NULL;
	if (iLength > 0)
	{
		pBuff = new T[iMaxSize];
		for (int i = 0; i < n; ++i)
			pBuff[i] = *(first + i);
	}
}

template<typename T>
bool CMyVector<T>::operator<=(CMyVector const& srcVector) const
{
	return !(*this > srcVector);
}

template<typename T>
bool CMyVector<T>::operator<(CMyVector const& srcVector) const
{
	return !(*this >= srcVector);
}

template<typename T>
bool CMyVector<T>::operator>=(CMyVector const& srcVector) const
{
	return (*this > srcVector || *this == srcVector);
}

template<typename T>
bool CMyVector<T>::operator>(CMyVector const& srcVector) const//strcmp的比较规则
{
	size_t minLen = iLength < srcVector.iLength ? iLength : srcVector.iLength;
	for (size_t i = 0; i < minLen; ++i)
	{
		if (pBuff[i] > srcVector.pBuff[i])
			return true;
		if (pBuff[i] < srcVector.pBuff[i])
			return false;
	}
	if (minLen == iLength)
		return false;
	return true;
}

template<typename T>
void CMyVector<T>::resize(int n, const T & elem)
{
	if (n > iLength)
	{
		iMaxSize = n;
		T *tempBuff = new T[iMaxSize];
		memcpy(tempBuff, pBuff, sizeof(T)* iLength);
		if (pBuff)
			delete[]pBuff;
		pBuff = tempBuff;
		for (size_t i = iLength; i < n; ++i)
		{
			pBuff[i] = elem;
		}
	}
	iLength = n;
}

template<typename T>
void CMyVector<T>::resize(int n)
{
	if (n > iLength)
	{
		iMaxSize = n;
		T *tempBuff = new T[iMaxSize];
		memcpy(tempBuff, pBuff, sizeof(T)* iLength);
		if (pBuff)
			delete[]pBuff;
		pBuff = tempBuff;
		memset(&pBuff[iLength], 0, sizeof(T)* (n - iLength));
	}
	iLength = n;
}

template<typename T>
void CMyVector<T>::pop_back()
{
	if (iLength > 0)
		iLength--;
}

template<typename T>
void CMyVector<T>::push_back(T const& elem)
{
	if (iLength >= iMaxSize)//证明容量满了
	{
		iMaxSize = iMaxSize + ((iMaxSize >> 1) > 1 ? (iMaxSize >> 1) : 1);
		T *tempBuff = new T[iMaxSize];
		memcpy(tempBuff, pBuff, sizeof(T)* iLength);
		if (pBuff)
			delete[]pBuff;
		pBuff = tempBuff;
	}
	pBuff[iLength++] = elem;
}

template<typename T>
T & CMyVector<T>::back()
{
	return pBuff[iLength - 1];
}

template<typename T>
T & CMyVector<T>::front()
{
	return pBuff[0];
}

template<typename T>
T & CMyVector<T>::operator[](int index) const
{
	return pBuff[index];
}

template<typename T>
T & CMyVector<T>::at(int index) const
{
	if (index >= 0 && index < iLength)
		return pBuff[index];
	throw "out_of_range";
}

template<typename T>
void CMyVector<T>::swap(CMyVector & srcVector)
{
	size_t tempLen = iLength;
	size_t tempMaxSize = iMaxSize;
	T *tempBuff = pBuff;

	iLength = srcVector.iLength;
	iMaxSize = srcVector.iMaxSize;
	pBuff = srcVector.pBuff;

	srcVector.iLength = tempLen;
	srcVector.iMaxSize = tempMaxSize;
	srcVector.pBuff = tempBuff;
}

template<typename T>
void CMyVector<T>::assign(int n, T const& elem)
{
	clear();//有可能自身已经有数据，防止内存泄露
	iLength = iMaxSize = n;
	pBuff = new T[iMaxSize];
	for (int i = 0; i < n; ++i)
		pBuff[i] = elem;
}

template<typename T>
void CMyVector<T>::reserve(int n)
{
	if (n > iMaxSize)
	{
		iMaxSize = n;//把最大大小扩大
		T * tempBuff = new T[iMaxSize];//用最大的空间去分配一个临时的数组
		memcpy(tempBuff, pBuff, sizeof(T)* iLength);//把当前数组的内容拷贝到这个临时数组
		if (pBuff)//如果当前数组有值，把当前数组的内容delete掉
			delete[] pBuff;
		pBuff = tempBuff;//把指向临时数据的指针传递给指向当前数组的指针，那么当前数组就有值了
	}
}

template<typename T>
bool CMyVector<T>::operator!=(CMyVector const& srcVector) const
{
	return !(*this == srcVector);
}

template<typename T>
bool CMyVector<T>::operator==(CMyVector const& srcVector) const//规则参照strcmp
{
	if (iLength != srcVector.iLength)
		return false;
	for (size_t i = 0; i < iLength; ++i)
	{
		if (pBuff[i] != srcVector.pBuff[i])
			return false;
	}
	return true;
}

template<typename T>
size_t CMyVector<T>::capacity() const
{
	return iMaxSize;
}

template<typename T>
bool CMyVector<T>::empty() const
{
	return iLength == 0;
	//return pBuff == NULL;
}

template<typename T>
size_t CMyVector<T>::size() const
{
	return iLength;
}

template<typename T>
CMyVector<T>::CMyVector(int n, T const& elem)
{
	iLength = iMaxSize = n;
	pBuff = new T[iMaxSize];
	for (int i = 0; i < n; ++i)
		pBuff[i] = elem;
}

template<typename T>
CMyVector<T>::CMyVector(int n)
{
	iLength = iMaxSize = n;
	pBuff = new T[iMaxSize];
	memset(pBuff, 0, sizeof(T)* iLength);//可以不写，原因是T不知道类型；也可以写，原因一般情况默认构造都是赋零值
}

template<typename T>
CMyVector<T>::CMyVector(CMyVector const& other)
{
	iLength = other.iLength;
	iMaxSize = other.iMaxSize;
	pBuff = NULL;
	if (iMaxSize != 0)
	{
		pBuff = new T[iMaxSize];
		memcpy(pBuff, other.pBuff, sizeof(T)* iLength);
	}
}

template<typename T>
void CMyVector<T>::clear()
{
	iLength = 0;
	iMaxSize = 0;
	if (pBuff)
		delete[]pBuff;
	pBuff = NULL;
}

template<typename T>
CMyVector<T>::~CMyVector()
{
	clear();
}

template<typename T>
CMyVector<T>::CMyVector()
{
	pBuff = NULL;
	iLength = iMaxSize = 0;
}
