// test03.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <vector>
using std::vector;
#include "MyVector.h"

//vector模拟的是一个动态数组，不管系统的内存模型，只需要按动态数组的特性去理解vector
//使用要导头文件（C++头文件，没有.h），和命名空间的开放
//vector是一个容器，里面可以存放数据，任意类型的数据，只需要能被拷贝
//容器的元素之间必然有次序，元素本身可序
//支持随机的存取，迭代器也是随机存取迭代器
//在末尾插入和删除元素特别方便，性能比较高，在中端或前端进行插入或删除，性能比较低
//vector是动态数组，所以会导致内存重分配，一旦发生内存重分配，和vector相关的指针，引用，迭代器都会失效(每次用迭代器的时候重新得到一次)
//内存重分配耗时
//在用的时候尽量避免重分配，或者每次用迭代器，指针，引用时，重新赋值
//at会进行范围检查，如果越界，抛异常
//vector只支持最低限度的异常处理，at是唯一被标准要求抛异常的函数
//如果vector调用的函数抛出异常，标准做出以下保证：
//1、如果push_back插入元素时发生异常，这个函数不起作用
//2、如果元素的拷贝不抛异常，insert要么成功，要么不起作用
//3、pop_back不抛异常

int _tmain(int argc, _TCHAR* argv[])
{
	vector<int> v;
	//v.reserve(100);
	v.push_back(1);
	vector<int>::iterator vit;
	vit = v.begin();
	v.insert(vit, 100);
	vit = v.begin();
	vit = v.insert(vit, 200);
	vit = v.insert(vit, 300);
	for (vit = v.begin(); vit != v.end(); ++vit)
		printf("%d\n",*vit);
	//vector<int> v(2,3);
	//vector<int>::iterator vit;
	//vector<int>::reverse_iterator rvit;//逆向迭代器
	//vit = v.begin() + 1;
	//v.insert(vit, 100);
	//for (vit = v.begin(); vit != v.end(); ++vit)
	//	printf("%d\n",*vit);
	//vector<int> v1(10, 2);
	//v.swap(v1);
	//swap(v, v1);
	printf("//====================\n");

	CMyVector<int> v1(2, 3);
	v1.push_back(1);
	CMyVector<int>::MyIterator vit1;
	vit1 = v1.begin() + 1;
	v1.insert(vit1, 100);
	for (vit1 = v1.begin(); vit1 != v1.end(); ++vit1)
		printf("%d\n",*vit1);
	return 0;
}

