#pragma once

//作业：1、把今天的函数自己写示例用一下；2、考虑一下_clear有没有bug，如果有原因是什么？

template <class T>
class CMyTree
{
	struct TreeNode//私有结构
	{
		T data;//数据域
		//指针域
		TreeNode *parent;//指向父亲的指针
		TreeNode *brother;//指向兄弟的指针
		TreeNode *child;//指向孩子的指针
	};
	TreeNode *pRoot;//根节点
public:
	CMyTree();
	~CMyTree();
	void clear();
	bool isFind(T const& findData);
	void insert(T const& findData, T const& insertData, bool isChild = false);
private:
	void _clear(TreeNode * root);//借助递归的特性来进行删除，递归需要有结束，结束通过参数来改变达成结束条件
	TreeNode * _find(TreeNode *root, T const& findData)
	{
		if (root)
		{
			if (root->data == findData)
				return root;
			TreeNode * tempNode = _find(root->brother,findData);
			if (tempNode)
				return tempNode;
			return _find(root->child,findData);
		}
		return NULL;
	}
};

template <class T>
void CMyTree<T>::insert(T const& findData, T const& insertData, bool isChild /*= false*/)
{
	//准备一个待插入节点
	TreeNode * tempInsertNode = new TreeNode;
	tempInsertNode->data = insertData;
	tempInsertNode->parent = NULL;
	tempInsertNode->brother = NULL;
	tempInsertNode->child = NULL;

	if (pRoot)
	{
		TreeNode *findNode = _find(pRoot, findData);//在非空树中找finddata这个数据所在的节点
		if (findNode)
		{
			//表示找到了插入位置
			if (isChild)
			{
				if (findNode->child)
				{
					//这个节点有子节点
					//描述有序树
					TreeNode *tempNode = findNode->child;
					while (tempNode->brother)
						tempNode = tempNode->brother;
					tempNode->brother = tempInsertNode;
					tempInsertNode->parent = tempNode->parent;
					//tempInsertNode->brother = findNode->child;
					//tempInsertNode->parent = findNode;
					//findNode->child = tempInsertNode;
				}
				else
				{
					//需要插入新节点的节点，这个节点没有子节点
					findNode->child = tempInsertNode;
					tempInsertNode->parent = findNode;
				}
			}
			else
			{
				if (findNode->brother)
				{
					TreeNode *tempNode = findNode->brother;
					while (tempNode->brother)
						tempNode = tempNode->brother;
					tempNode->brother = tempInsertNode;
					tempInsertNode->parent = tempNode->parent;
				}
				else
				{
					findNode->brother = tempInsertNode;
					tempInsertNode->parent = findNode->parent;
				}
			}
		}
		else
		{
			//代表非空树中没有找到findData这个节点，自定义一个插入的规则
			//如果没有找到待操作节点，把待插入节点，放在子节点的子节点
			TreeNode *tempNode = pRoot;
			while (tempNode->child)
				tempNode = tempNode->child;
			tempNode->child = tempInsertNode;
			tempInsertNode->parent = tempNode;
		}
	}
	else
		pRoot = tempInsertNode;
}

template <class T>
bool CMyTree<T>::isFind(T const& findData)
{
	return _find(pRoot, findData) != NULL;
}

template <class T>
void CMyTree<T>::_clear(TreeNode * root)
{
	if (root)
	{
		_clear(root->brother);
		_clear(root->child);
		delete root;
		root = NULL;
	}
}

template <class T>
CMyTree<T>::~CMyTree()
{
	clear();
}

template <class T>
CMyTree<T>::CMyTree()
{
	pRoot = NULL;
}
