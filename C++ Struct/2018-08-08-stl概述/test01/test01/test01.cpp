// test01.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"

#include <memory.h>
//stl的概述
//标准模板库
//程序员不需要了解stl的原理，便可以享受数据结构及数据算法所带来的成果


//template <class T>
//void select_sort(T arr[], size_t len)
//{
//	for (int i = 0; i < len - 1; ++i)
//	{
//		for (int j = i + 1; j < len; ++j)
//		{
//			if (arr[i] > arr[j])
//			{
//				T temp = arr[i];
//				arr[i] = arr[j];
//				arr[j] = temp;
//			}
//		}
//	}
//}

//容器
//序列式容器  可序  vector deque list
//关系式容器  已序  set map  multiset multimap
//已序-》是容器的已序，关注还是在容器，已序只不过是其中的一个功能

//stl容器  增删改查  容器的接口是相同 （得到容器元素个数，容器是否为空，容器的关系判断，删除，插入，清除）

//stl容器必须满足3个条件
//1、容器进行元素的插入操作时，内部实现的是拷贝操作，把数据备份一份放在容器中。stl容器的所有元素都必须能够被拷贝
//2、不管是可序还是已序，所有的元素形成次序，容器没有做操作的情况下，多次遍历这个容器，元素次序是一致的
//3、一般而言，容器的各项操作并非绝对安全

//迭代器
//可遍历stl容器内全部或部分元素的对象，用来指出容器中的一个特定的位置
//迭代器：*，++，==，！=，=
//迭代器就是 一个所谓的智能指针，具备遍历复杂数据结构的能力
//迭代器和容器相关联，每一个容器都有自己的迭代器，每一个容器都 把自己的迭代器嵌套在自己的容器内部
//容器里面有接口来辅助迭代器，begin，end，这两个函数可以得到容器的第一个元素的位置及最后一个元素的下一个位置
//stl中迭代器也叫逾尾迭代器
//好处：1、遍历元素的时候，循环结束的时机的把握，不需要知道容器元素个数
//		2、不需要对空区间采取特殊的处理手法，begin == end，代表空区间

template <class T>
class CMyStack
{
	T buff[10];
	int	buffTop;
public:
	CMyStack()
	{
		memset(buff, 0, sizeof(buff));//内存设置值
		buffTop = 0;
	}
public:
	void push(T const & srcData)
	{
		buff[buffTop++] = srcData;
	}
	T GetTop()
	{
		//if (buffTop > 0 && buffTop < 10)
			return buff[buffTop - 1];
		//throw "out";
	}
};

int _tmain(int argc, _TCHAR* argv[])
{
	//CMyStack<int> ms;
	//printf("%d\n", ms.GetTop());
	//int a;
	//memset(&a, 1, sizeof(a));
	//printf("%d\n",a);
	//int arrInt[4] = { 3, 1, 6, 2 };
	//select_sort(arrInt, 4);
	//for (int i = 0; i < 4; ++i)
	//	printf("%d\n",arrInt[i]);

	//float arrFloat[4] = { 0.2f, 1.3f, 3.2f, 1.8f };
	//select_sort(arrFloat, 4);
	//for (int i = 0; i < 4; ++i)
	//	printf("%f\n", arrFloat[i]);
	return 0;
}

