#include <cstdio>
#include <cstring>
#define TEST_LEN 20


void make_next(const char* pattern, int* next)
{
    int patt_len = strlen(pattern);

    next[0] = -1;

    int q, k;
    for(q = 1, k = 0; q < patt_len; q++)
    {
        while(k > 0 && pattern[q] != pattern[k])
        {
            k = next[k - 1];
        }

        if(pattern[q] == pattern[k])
        {
            k++;
        }

        next[q] = k;
    }
}

int kmp(const char* testString, const char* pattern, int* next)
{
    int test_len = strlen(testString);
    int patt_len = strlen(pattern);

    make_next(pattern, next);

    int i, q;
    for(i = 0, q = 0; i < test_len; i++)
    {
        while(q > 0 && testString[i] != pattern[q])
        {
            q = next[q - 1];
        }

        if(pattern[q] == testString[i])
        {
            q++;
        }
        
        if(q == patt_len)
        {
            break;
        }
    }

    return i - q + 1;
}

int main(int argc, char **argv)
{

    char *text = "ababxbababababcdababcabddcadfdsss";
    char *pattern = "abcabd";

    int next[20] = {0};

    int index_match = kmp(text, pattern, next);

    printf("match pattern: %d", index_match);

    for (int i = 0; i < strlen(pattern); i++)
    {
        printf("%4d", next[i]);
    }

    return 0;

}
