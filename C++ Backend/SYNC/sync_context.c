#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/epoll.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <errno.h>
#include <time.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>

#define HTTP_VERSION    "HTTP/1.1"
#define USER_AGENT      "User-Agent: Mozilla/5.0 (Windows NT 5.1; rv:10.0.2) Gecko/20100101 Firefox/10.0.2\r\n"
#define ENCODE_TYPE     "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\n"
#define CONNECTION_TYPE "Connection: close\r\n"


#define BUFFER_SIZE     4096
#define HOSTNAME_LENGTH 128
#define ASYNC_CLIENT_NUM 1024

typedef void (*async_result_cb)(const char *hostname, const char *result);

typedef struct _async_context       //异步struct
{
    pthread_t thread_id;
    int epoll_fd;
}async_context;

typedef struct _http_request        //存储http连接的主机名和资源
{
    char *hostname;
    char *resource;
}http_request;

typedef struct _ep_arg
{
    int sockfd;
    char hostname[HOSTNAME_LENGTH];
    async_result_cb cb;
}ep_arg;

//API
http_request reqs[] = {
    {"api.seniverse.com", "/v3/weather/now.json?key=0pyd8z7jouficcil&location=beijing&language=zh-Hans&unit=c"},
    {"api.seniverse.com", "/v3/weather/now.json?key=0pyd8z7jouficcil&location=changsha&language=zh-Hans&unit=c"},
    {"api.seniverse.com", "/v3/weather/now.json?key=0pyd8z7jouficcil&location=shenzhen&language=zh-Hans&unit=c"},
    {"api.seniverse.com", "/v3/weather/now.json?key=0pyd8z7jouficcil&location=shanghai&language=zh-Hans&unit=c"},
    {"api.seniverse.com", "/v3/weather/now.json?key=0pyd8z7jouficcil&location=tianjin&language=zh-Hans&unit=c"},
    {"api.seniverse.com", "/v3/weather/now.json?key=0pyd8z7jouficcil&location=wuhan&language=zh-Hans&unit=c"},
    {"api.seniverse.com", "/v3/weather/now.json?key=0pyd8z7jouficcil&location=hefei&language=zh-Hans&unit=c"},
    {"api.seniverse.com", "/v3/weather/now.json?key=0pyd8z7jouficcil&location=hangzhou&language=zh-Hans&unit=c"},
    {"api.seniverse.com", "/v3/weather/now.json?key=0pyd8z7jouficcil&location=nanjing&language=zh-Hans&unit=c"},
    {"api.seniverse.com", "/v3/weather/now.json?key=0pyd8z7jouficcil&location=jinan&language=zh-Hans&unit=c"},
    {"api.seniverse.com", "/v3/weather/now.json?key=0pyd8z7jouficcil&location=taiyuan&language=zh-Hans&unit=c"},
    {"api.seniverse.com", "/v3/weather/now.json?key=0pyd8z7jouficcil&location=wuxi&language=zh-Hans&unit=c"},
    {"api.seniverse.com", "/v3/weather/now.json?key=0pyd8z7jouficcil&location=suzhou&language=zh-Hans&unit=c"},
};

//hostent结构体
// struct hostent
// {
//     char *h_name;       /* official name of host */
//     char **h_aliases;   /* alias list */
//     int h_addrtype;     /* host address type */
//     int h_length;       /* length of address */
//     char **h_addr_list; /* list of addresses */
// }
char *host_to_ip(const char *hostname)
{
    struct hostent *host_entry = gethostbyname(hostname);
    if(host_entry)
    {
        return inet_ntoa(*(struct in_addr *)*host_entry->h_addr_list);
    }
    else
    {
        return NULL;
    }
}

void *http_async_client_callback(void *arg)
{
    async_context *ctx = (async_context *)arg;
    int epoll_fd = ctx->epoll_fd;

    while(1)
    {
        struct epoll_event events[ASYNC_CLIENT_NUM] = {0};

        int ready_num = epoll_wait(epoll_fd, events, ASYNC_CLIENT_NUM, -1);
        if (ready_num < 0)
        {
            if(errno == EINTR || errno == EAGAIN)
            {
                continue;
            }
            else
            {
                break;
            }
        }
        else if (ready_num == 0)
        {
            continue;
        }
        printf("ready_num:%d\n", ready_num);

        for (int i = 0; i < ready_num; i++)
        {
            ep_arg *data = (ep_arg *)events[i].data.ptr;
            int sockfd = data->sockfd;

            char buffer[BUFFER_SIZE] = {0};
            struct sockaddr_in addr;
            size_t addr_len = sizeof(struct sockaddr_in);
            int _recv = recv(sockfd, buffer, BUFFER_SIZE, 0);

            data->cb(data->hostname, buffer);

            int ret = epoll_ctl(epoll_fd, EPOLL_CTL_DEL, sockfd, NULL);

            close(sockfd);

            free(data);
        }
    }
}

async_context *http_async_client_init(void)     //初始化
{
    int epoll_fd = epoll_create(1);
    if(epoll_fd < 0)
    {
        return NULL;
    }
    async_context *ctx = (async_context *)calloc(1, sizeof(async_context));
    if(ctx == NULL)
    {
        close(epoll_fd);
        return NULL;
    }

    ctx->epoll_fd = epoll_fd;

    int ret = pthread_create(&ctx->thread_id, NULL, http_async_client_callback, ctx);

    if(ret)
    {
        perror("pthread_create");
        return NULL;
    }

    usleep(1);      //子线程先行

    return ctx;
}

int http_async_context_uninit(async_context *ctx)
{
    close(ctx->epoll_fd);
    pthread_cancel(ctx->thread_id);

    return 0;
}

int http_async_context_commit(async_context *ctx,
                              const char *hostname,
                              const char *resource,
                              async_result_cb cb)
{
    char *ip = host_to_ip(hostname);
    if(ip == NULL)
    {
        return -1;
    }

    int sockfd = socket(AF_INET, SOCK_STREAM, 0);

    struct sockaddr_in sin = {0};
    sin.sin_addr.s_addr = inet_addr(ip);
    sin.sin_port = htons(80);
    sin.sin_family = AF_INET;

    if(connect(sockfd, (struct sockaddr*)&sin, sizeof(struct sockaddr_in)) == -1)
    {
        return -1;
    }

    fcntl(sockfd, F_SETFL, O_NONBLOCK);

    char buffer[BUFFER_SIZE] = {0};

    int len = sprintf(buffer,
"GET %s %s\r\n\
Host: %s\r\n\
%s\r\n\
\r\n",
        resource, HTTP_VERSION,
        hostname,
        CONNECTION_TYPE);

    printf("request buffer:%s\n", buffer);
    int slen = send(sockfd, buffer, strlen(buffer), 0);

    ep_arg *arg = (ep_arg *)calloc(1, sizeof(ep_arg));

    if(arg == NULL)
    {
        return -1;
    }

    arg->sockfd = sockfd;
    arg->cb = cb;

    struct epoll_event ev;
    ev.data.ptr = arg;
    ev.events = EPOLLIN;

    int ret = epoll_ctl(ctx->epoll_fd, EPOLL_CTL_ADD, sockfd, &ev);

    return ret;
}

static void http_async_client_result_callback(const char *hostname, const char *result)
{
    printf("hostname:%s, result:%s\n\n\n\n", hostname, result);
}

int main(int argc, char *argv[])
{
    async_context *ctx = http_async_client_init();
    if (ctx == NULL)
        return -1;

    int count = sizeof(reqs) / sizeof(reqs[0]);
    int i = 0;
    for (i = 0; i < count; i++)
    {
        http_async_context_commit(ctx, reqs[i].hostname, reqs[i].resource, http_async_client_result_callback);
    }

    getchar();
}