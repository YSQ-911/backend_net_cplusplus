#include <iostream>
#include <list>

using namespace std;

class Subject;

class Observer
{
 public:
    Observer(){}
    virtual ~Observer(){}
    virtual void update(Subject *subject) = 0;
    virtual void update(string content) = 0;
};

class Subject
{
 public:
    Subject(){}
    virtual ~Subject(){};
    virtual void setContent(string _content) = 0;
    virtual string getContent() = 0;
    virtual string getAbstractContent() = 0;

    //订阅
    void attach(Observer *observer)
    {
        observers.push_back(observer);
    }

    //取消订阅
    void detach(Observer *observer)
    {
        observers.remove(observer);
    }
    
 protected:
    //通知所有订阅者
    void notifyObservers()
    {
        for(Observer *reader : observers)
        {
            reader->update(this);   //拉模型
        }
    }

    //通知所有订阅者
    void notifyObservers(string _content)
    {
        for(Observer *reader : observers)
        {
            reader->update(_content);   //推模型
        }
    }
    
 private:
    list<Observer *> observers;
};


class Reader : public Observer
{
 public:
    Reader(){}
    ~Reader(){}

    void update(Subject *subject)
    {
        cout << m_username << "订阅的消息内容：" << subject->getContent() << endl;
    }

    //推模型（粒度更大）
    void update(string _content)
    {
        cout << m_username << "订阅的消息内容：" << _content << endl;
    }

    string getUsername()
    {
        return m_username;
    }

    void setUsername(string _username)
    {
        m_username = _username;
    }
 private:
    string m_username;
};


class Push : public Subject
{
 public:
    Push(){}
    ~Push(){}
    
    void setContent(string _content)
    {
        m_content = _content;
        notifyObservers();
    }

    string getContent()
    {
        return m_content;
    }

    string getAbstractContent()
    {
        return "push information!";
    }

  private:
    string m_content;
};


int main()
{
    Push *push = new Push();
    //Push *push = new Push();

    Reader *reader_1 = new Reader();
    reader_1->setUsername("one user");

    Reader *reader_2 = new Reader();
    reader_2->setUsername("second user");

    push->attach(reader_1);
    push->setContent("first push");

    printf("-------------------------------------\n");
    push->attach(reader_2);
    push->setContent("second push");
    push->getAbstractContent();

    printf("-----------------------------------\n");
    return 0;
}
