#include <iostream>
#include <mutex>
#include <thread>
#include <chrono>

using namespace std;

#define SWITCH 2

// =default: 用于显式要求编译器提供合成版本的四大函数(构造、拷贝、析构、赋值)
/* =delete: 用于定义删除函数，在旧标准下，我们如果希望阻止拷贝可以通过显式声明
            拷贝构造函数和拷贝赋值函数为private，但新标准下允许我们定义删除函数*/



#if SWITCH == 1     //饿汉式单例模式
class Singleton
{
 public:
    static Singleton *getInstance()
    {
        static Singleton _singleton;
        return &_singleton;
    }
 private:
    Singleton() = default;
    Singleton(const Singleton& s) = delete;
    Singleton& operator=(const Singleton& s) = delete;
};

#elif SWITCH == 2     //饿汉式单例模式,但是构造在main函数初始化时，保证安全
class Singleton
{
 public:
    static Singleton *getInstance()
    {
        return &m_singleton;
    }
 private:
    static Singleton m_singleton;
    Singleton()
    {
        cout << "Singleton constructor!" << endl;
    }
    Singleton(const Singleton& s) = delete;
    Singleton& operator=(const Singleton& s) = delete;
};
Singleton Singleton::m_singleton;


#elif SWITCH == 3   //原始懒汉式单例模式
class Singleton
{
 public:
    static Singleton *getInstance()
    {
        if(Singleton::_singleton == nullptr)
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
            _singleton = new Singleton();
        }
        return _singleton;
    }

 private:
    static Singleton *_singleton;
    Singleton() = default;
    Singleton(const Singleton& s) = delete;
    Singleton& operator=(const Singleton& s) = delete;

    class GarbageCollector
    {
     public:
        ~GarbageCollector()
        {
            cout << "~GarbageCollector" << endl;

            if(Singleton::_singleton)
            {
                cout << "free _singleton" << endl;
                delete Singleton::_singleton;
                Singleton::_singleton = nullptr;
            }
        }
    };

    static GarbageCollector m_gc;
};

Singleton *Singleton::_singleton = nullptr;
Singleton::GarbageCollector Singleton::m_gc;



#elif SWITCH == 4       //线程安全的懒汉单例模式
class Singleton
{
 public:
    static Singleton *getInstance()
    {
        m_mutex.lock();                     //加锁保证线程安全
        if(Singleton::_singleton == nullptr)
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
            _singleton = new Singleton();
        }
        m_mutex.unlock();                   ////解锁
        return _singleton;
    }

 private:
    static Singleton *_singleton;
    static mutex m_mutex;
    Singleton() = default;
    Singleton(const Singleton& s) = delete;
    Singleton& operator=(const Singleton& s) = delete;

    class GarbageCollector
    {
     public:
        ~GarbageCollector()
        {
            cout << "~GarbageCollector" << endl;

            if(Singleton::_singleton)
            {
                cout << "free _singleton" << endl;
                delete Singleton::_singleton;
                Singleton::_singleton = nullptr;
            }
        }
    };

    static GarbageCollector m_gc;
    
};

Singleton *Singleton::_singleton = nullptr;
Singleton::GarbageCollector Singleton::m_gc;
mutex Singleton::m_mutex;



#elif SWITCH == 5       //双锁型单例
class Singleton
{
 public:
    static Singleton *getInstance()
    {
        if(Singleton::_singleton == nullptr)
        {
            m_mutex.unlock();
            if(Singleton::_singleton == nullptr)
            {
                std::this_thread::sleep_for(std::chrono::milliseconds(10));
                _singleton = new Singleton();
            }
            m_mutex.unlock();                   //解锁
        }
        return _singleton;
    }

 private:
    static Singleton *_singleton;
    static mutex m_mutex;
    Singleton() = default;
    Singleton(const Singleton& s) = delete;
    Singleton& operator=(const Singleton& s) = delete;

    class GarbageCollector
    {
     public:
        ~GarbageCollector()
        {
            cout << "~GarbageCollector" << endl;

            if(Singleton::_singleton)
            {
                cout << "free _singleton" << endl;
                delete Singleton::_singleton;
                Singleton::_singleton = nullptr;
            }
        }
    };

    static GarbageCollector m_gc;
    
};
Singleton *Singleton::_singleton = nullptr;
Singleton::GarbageCollector Singleton::m_gc;
mutex Singleton::m_mutex;

#elif SWITCH == 6       //原子操作


#endif

static mutex single_lock;

void print_address()
{
    Singleton *singleton = Singleton::getInstance();

    single_lock.lock();

    cout << singleton << endl;

    single_lock.unlock();
}


void thread_Singleton()
{
    thread threads[10];

    for(auto&t : threads)
    {
        t = thread(print_address);
    }

    for(auto&t : threads)
    {
        t.join();
    }
}

int main()
{
    thread_Singleton();

    cout << endl << Singleton::getInstance() << endl;
}
