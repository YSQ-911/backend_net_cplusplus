#include <iostream>

class Solution
{
public:
    bool hasPath(char *matrix, int rows, int cols, char *str)
    {
        if (matrix == nullptr || rows <= 0 || cols <= 0 || str == nullptr)
        {
            return false;
        }

        bool *visited = new bool[rows * cols];
        memset(visited, 0, rows * cols);
        for (int _rows = 0; _rows < rows; _rows++)
        {
            for (int _cols = 0; _cols < cols; _cols++)
            {
                if (judge(matrix, rows, cols, _rows, _cols, visited, str))
                {
                    return true;
                }
            }
        }
        return false;
    }

    bool judge(char *matrix, int rows, int cols, int _rows, int _cols, bool *visited, char *str)
    {
        if(*str == '\0')
        {
            return true;
        }

        if(_cols < 0 || _cols >= cols || _rows < 0 || _rows >= rows)
        {
            return false;
        }

        if(visited[_rows * rows + _cols] || *str != matrix[_rows * cols + _cols])
        {
            return false;
        }
        visited[_rows * rows + _cols] = true;
        
        bool sign = judge(matrix, rows, cols, _rows - 1, _cols, visited, str + 1)
                    || judge(matrix, rows, cols, _rows + 1, _cols, visited, str + 1)
                    || judge(matrix, rows, cols, _rows, _cols - 1, visited, str + 1)
                    || judge(matrix, rows, cols, _rows, _cols + 1, visited, str + 1);

        visited[_rows * rows + _cols] = false;

        return sign;
    }
};