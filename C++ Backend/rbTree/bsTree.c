#include <stdio.h>
#include <stdlib.h>

#define int KEY_VALUE

//#define BSTREE_ENTRY(name, type)
//    struct name{
//        struct type *left;
//        struct type *left;
//    }

struct bstree_node
{
    KEY_TYPE data;
    //BSTREE_ENTRY(, bstree_node) bst;
    struct bstree_node *left;
    struct bstree_node *right;
};

struct bstree
{
    struct bstree_node *root;
    struct bstree_node *nil;
};


//树节点创建
struct bstree_node *bstree_create_node(KEY_TYPE key)
{
    struct bstree_node *node = (struct bstree_node *)malloc(sizeof(struct bstree_node));
    if(node == NULL)
    {
        return NULL;
    }

    node->data = key;

    node->left = node->right = NULL;
    
    return node;
}

//树插入
int bstree_insert(struct bstree *T, KEY_TYPE key) 
{
    if(T == NULL)
    {
        return NULL;
    }

    if(T->root)
    {
        T->root = bstree_create_node(key);
        return 0;
    }

    struct bstree_node *node = T->root;
    struct bstree_node *tmp = node;


    while (node)
    {
        if(key < node->data)
        {
            node = node->left;
        }
        else if(key > node->data)
        {
            node = node->right;
        }
        else
        {
            printf("exit!!!\n");
            return -2;
        }
    }

    if(key < tmp->data)
    {
        tmp->left = bstree_create_node(key);
    }
    else
    {
        tmp->right = bstree_create_node(key);
    }
}

//遍历树
int bstree_traversal(struct bstree_node *node)
{
    if(node == NULL)
    {
        return 0;
    }

    bstree_traversal(node->left);
    printf("%4d", node->data);
    bstree_traversal(node->right);
    printf("%4d", node->data);
}

//
struct bstree *min_node(struct bstree_node *node)
{
    if(node->left == NULL)
    {
        return node;
    }
    return min_node(node->left);
}

//
struct bstree *max_node(struct bstree_node *node)
{
    if(node->right == NULL)
    {
        return NULL;
    }
    return max_node(node->right);
}

//删除最小节点
struct bstree_node *min_node_delete(struct bstree_node *node)
{
    if(node->left == NULL)
    {
        struct bstree_node *right_root = node->right;
        delete node;
        return right_root;
    }
    node->left = min_node_delte(node->left);
    return node;
}

//删除节点
struct bstree *bstree_delete(struct bstree *T, KEY_TYPE key)
{
    if(T == NULL)
    {
        return;
    }

    struct bstree_node *node = T->root;
    //struct bstree_node *tmp = node;

    while(node)
    {
        if(key == node->data)
        {
            if(node->left == NULL)
            {
                struct bstree_node *node_right = node->right;
                delete node;
                return node_right;
            }
            else if(node->right == NULL)
            {
                struct bstree_node *node_left = node->left;
                delete node;
                return node_left;
            }
            else
            {
                struct bstree_node *node_replace = (struct bstree_node *)malloc(sizeof(struct bstree_node));
                node_replace->right = min_node_delete(node->right);

                node_replace->left = node->left;
                delete node;
                
                return node_replace;
            }
        }
        else if(key > node->data)
        {
            node = node->right;
        }
        else
        {
            node = node->left;
        }
    }
}




int main()
{
    int keyArray[20] = {24,25,13,35,23, 26,67,47,38,98, 20,19,17,49,12, 21,9,18,14,15};
    for(int i = 0; i < 20; i++)
    {
}
