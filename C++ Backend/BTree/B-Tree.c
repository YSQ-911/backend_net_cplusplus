#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DEGREE 3        //阶数为3

typedef int KEY_VALUE;

typedef struct _btree_node
{
    KEY_VALUE *keys;        //保存结点数据
    struct _btree_node **childrens;     //存储孩子结点，孩子结点是个一维数组
    int num;        //当前结点有多少个结点
    int leaf;       //是否是叶子结点
} btree_node;

typedef struct _btree
{
    btree_node *root;
    int degree;     //阶数
} btree;

//创建结点分配内存
btree_node *btree_create_node(int degree, int leaf)
{
    btree_node *node = (btree_node *)calloc(1, sizeof(btree_node));
    if(node == NULL)
    {
        return NULL;
    }

    node->leaf = leaf;
    node->num = 0;
    node->keys = (KEY_VALUE *)calloc(1, (2 * degree - 1) * sizeof(KEY_VALUE));      //每个结点至少有
    node->childrens = (btree_node **)calloc(1, (2 * degree) * sizeof(btree_node));   //每个结点最多有2M个结点

    return node;
}

//结点销毁
void btree_destroy_node(btree_node *node)
{
    if(node == NULL)
    {
        return ;
    }

    free(node->keys);
    free(node->childrens);
    free(node);
}

void btree_create(btree *T, int degree)
{
    T->degree = degree;
    
    btree_node *node = btree_create_node(degree, 1);
    T->root = node;
}

//遍历
void btree_traverse(btree_node *node)
{
    if(node == NULL)
    {
        printf("Tree is empty\n");
        return;
    }

    int loop = 0;

    for(loop; loop < node->num; loop++)
    {
        if(node->leaf == 0)
        {
            btree_traverse(node->childrens[loop]);
        }
        printf("%c", node->keys[loop]);
    }

    if(node->leaf == 0)
    {
        btree_traverse(node->childrens[loop]);
    }
}

//搜索
int btree_search(btree_node *node, int low, int high, KEY_VALUE key)
{
    if(high < low || node == NULL || low < 0 || high < 0)
    {
        return 0;
    }

    int mid = 0;
    //二分法搜索
    while(low <= high)
    {
        mid = (high - low) / 2;
        if(node->keys[mid] > key)
        {
            high = mid - 1;
        }
        else if(node->keys[mid] < key)
        {
            low = mid + 1;
        }
    }

    return low;
}

//结点分裂
void btree_split(btree *T, btree_node *node, int child_idx)
{
    int _degree = T->degree;

    btree_node *cur = node->childrens[child_idx];       //当前结点
    btree_node *new_node = btree_create_node(_degree, cur->leaf);

    new_node->num = _degree - 1;

    for(int i = 0; i < _degree - 1; i++)
    {
        new_node->keys[i] = cur->keys[i + _degree];
    }

    //当前结点不是叶子结点
    if (cur->leaf == 0)
    {
        for(int j = 0; j < _degree; j++)
        {
            new_node->childrens[j] = cur->childrens[j + _degree];   //把原来的孩子结点移动到新的的父节点
        }
    }

    cur->num = _degree - 1;
    //new_node->num = _degree - 1;
    for(int i = node->num; i >= child_idx + 1; i--)
    {
        node->childrens[i + 1] = cur->childrens[i];
    }

    node->childrens[child_idx + 1] = new_node;

    for(int j = node->num - 1; j >= child_idx; j--)
    {
        node->keys[j + 1] = cur->keys[j];
    }
    node->keys[child_idx] = cur->keys[_degree - 1];
    node->num += 1;
}

//插入结点不满的情况
void btree_insert_nonfull(btree *T, btree_node *node, KEY_VALUE key)
{
    int insert_idx = node->num - 1; //当前结点要插入的位置

    //当结点是叶子结点
    if(node->leaf == 1)
    {
        //因为是顺序排列所以要寻找合适的位置插入
        while (insert_idx >= 0 && node->keys[insert_idx] > key)
        {
            node->keys[insert_idx + 1] = node->keys[insert_idx];
            insert_idx--;
        }
        node->keys[insert_idx + 1] = node->keys[insert_idx];
        node->num += 1;
    }
    else
    {

        while (insert_idx >= 0 && node->keys[insert_idx] > key)
        {
            insert_idx--;
        }

        //当要插入的根节点位置满了的话，进行裂变
        if (node->childrens[insert_idx + 1]->num == (2 * (T->degree)) - 1)
        {
            btree_split(T, node, insert_idx + 1);   //裂变
            if (key > node->keys[insert_idx + 1])
            {
                insert_idx++;
            }
        }

        btree_insert_nonfull(T, node->childrens[insert_idx + 1], key);
    }
}

//结点插入
void btree_insert(btree *T, KEY_VALUE key)
{
    btree_node *_root = T->root;
    if(_root->num == ((2 * T->degree) - 1))      //根节点满了的话进行分裂
    {
        btree_node *node = btree_create_node(T->degree, 0);
        T->root = node;

        node->childrens[0] = _root;
        btree_split(T, node, 0);

        int i = 0;
        if(node->keys[0] < key)
        {
            i++;
        }

        btree_insert_nonfull(T, node->childrens[i], key);
    }
    else
    {
        btree_insert_nonfull(T, _root, key);
    }
}

//结点归并
void btree_merge(btree *T, btree_node *node, int index)
{
    btree_node *left = node->childrens[index];
    btree_node *right = node->childrens[index + 1];

    int i = 0;
    left->keys[T->degree - 1] = node->keys[index];
    for(i = 0; i < T->degree - 1; i++)
    {
        left->keys[T->degree + i] = right->keys[i];
    }

    if(!left->leaf)
    {
        for(i = 0; i < T->degree; i++)
        {
            left->childrens[T->degree + i] = right->childrens[i];
        }
    }
    left->num += T->degree;

    btree_destroy_node(right);

    for(i = index + 1; i < node->num; i++)
    {
        node->keys[i - 1] = node->keys[i];
        node->childrens[i] = node->childrens[i + 1];
    }
    node->childrens[i + 1] = NULL;
    node->num -= 1;

    if(node->num == 0)
    {
        T->root = left;
        btree_destroy_node(node);
    }
}


//
void btree_delete_key(btree *T, btree_node *node, KEY_VALUE key)
{
    if(node == NULL)
    {
        return ;
    }

    int index = 0;
    int i = 0;

    while(node->keys[index] < key && index < node->num)
    {
        index++;
    }

    
    if(index < node->num && key == node->keys[index])       //
    {
        if(node->leaf)  //在叶子结点上的话直接删除
        {
            for(i = 0; i < node->num - 1; i++)
            {
                node->keys[i] = node->keys[i + 1];
            }

            node->keys[node->num - 1] = 0;
            node->num--;

            if(node->num == 0)
            {
                free(node);
                T->root = NULL;
            }
        }
        else if (node->childrens[index]->num > T->degree - 1) //前节点大于degree - 1
        {
            btree_node *front = node->childrens[index];   //
            node->keys[index] = front->keys[front->num - 1];

            btree_delete_key(T, front, front->keys[front->num - 1]);
        }
        else if (node->childrens[index + 1]->num > T->degree - 1) //后节点大于degree - 1
        {
            btree_node *after = node->childrens[index + 1];
            node->keys[index] = after->keys[0];

            btree_delete_key(T, after, after->keys[0]);
        }
        else    //前节点等于degree - 1 , 后节点也等于degree - 1
        {
            btree_merge(T, node, index);
            btree_delete_key(T, node->childrens[index], key);
        }
    }
    else
    {
        btree_node *child = node->childrens[index];
        if(child == NULL)
        {
            return ;
        }

        if(child->num == T->degree - 1)
        {
            btree_node *front = NULL;
            btree_node *after = NULL;

            if(index - 1 >= 0)
            {
                front = node->childrens[index - 1];
            }

            if(index + 1 <= node->num)
            {
                after = node->childrens[index + 1];
            }

            if((front && front->num >= T->degree) || 
                (after && after->num >= T->degree))
            {
                int _leaf = 0;
                if(front)
                    _leaf = 1;

                if(after && front)
                    _leaf = (after->num > front->num) ? 1 : 0;

                if (after && after->num >= T->degree && _leaf)
                {
                    child->keys[child->num] = node->keys[index];
                    child->childrens[child->num + 1] = after->childrens[0];
                    child->num++;

                    node->keys[index] = after->keys[0];
                    for (i = 0; i < after->num - 1; i++)
                    {
                        after->keys[i] = after->keys[i + 1];
                        after->childrens[i] = after->childrens[i + 1];
                    }

                    after->keys[after->num - 1] = 0;
                    after->childrens[after->num - 1] = after->childrens[after->num];
                    after->childrens[after->num] = NULL;
                    after->num--;
                }
                else
                {
                    for(i = child->num; i > 0; i--)
                    {
                        child->keys[i] = child->keys[i - 1];
                        child->childrens[i + 1] = child->childrens[i];
                    }

                    child->childrens[1] = child->childrens[0];
                    child->childrens[0] = front->childrens[front->num];
                    child->keys[0] = node->keys[index - 1];
                    child->num++;

                    front->keys[front->num - 1] = 0;
                    front->childrens[front->num] = NULL;
                    front->num--;
                }
                
            }
            else if((!front || front->num == T->degree - 1)
                && (!after || after->num == T->degree - 1))
            {
                if(front && front->num == T->degree - 1)
                {
                    btree_merge(T, node, index - 1);
                    child = front;
                }
                else if(after && after->num == T->degree - 1)
                {
                    btree_merge(T, node, index);
                }
            }
        }
        btree_delete_key(T, child, key);
    }
}

//结点删除
void btree_delete(btree *T, KEY_VALUE key)
{
    if(!T->root)
    {
        return ;
    }

    btree_delete_key(T, T->root, key);
}

//打印结点
void btree_print(btree *T, btree_node *node, int layer)
{
    btree_node *tmp = node;
    int i;

    if (tmp)
    {
        printf("\nlayer = %d keynum = %d is_leaf = %d\n", layer, tmp->num, tmp->leaf);
        for(i = 0; i < node->num; i++)
        {
            printf("%c", tmp->keys[i]);
        }
        printf("\n");

        layer++;        //层++
        for(i = 0; i <= tmp->num; i++)
        {
            if(tmp->childrens[i])
            {
                btree_print(T, tmp->childrens[i], layer);
            }
        }
    }
    else
    {
        printf("tree is empty\n");
    }
}

int main()
{
    btree T = {0};
    btree_create(&T, DEGREE);

    char key[26] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    for(int i = 0; i < 26; i++)
    {
        printf("%c ", key[i]);
        btree_insert(&T, key[i]);
    }


    btree_print(&T, T.root, 0);
    //btree_traverse(T.root);

    for(int j = 0; j < 26; j++)
    {
        printf("\n----------------------------------\n");
        btree_delete(&T, key[25 - j]);
        ///btree_print(&T, T.root, 0);
    }
    btree_traverse(T.root);
    return 0;
}
