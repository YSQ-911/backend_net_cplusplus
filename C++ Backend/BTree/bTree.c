#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DEGREE      3

typedef int KEY_VALUE;

typedef struct bTree_node
{
    KEY_VALUE *keys;
    struct bTree_node **childrens;
    int num;    
    int leaf;
}btree_node;

typedef struct bTree
{
    btree_node *root;
    int degree;     //阶数
}btree;


//创建结点
btree_node *btree_create_node(int degree, int leaf)
{
    btree_node *node = (btree_node *)malloc(sizeof(btree_node));
    memset(node, 0, sizeof(btree_node));

    if(node == NULL)
    {
        return NULL;
    }

    node->leaf = leaf;
    node->keys = (KEY_VALUE *)calloc(1, (2 * degree - 1) *sizeof(KEY_VALUE));
    node->childrens = (btree_node **)calloc(1, (2 * degree) * sizeof(btree_node));
    node->num = 0;

    return node;
}

//结点销毁
void btree_destory_node(btree_node *node)
{
    if(node == NULL)
    {
        return ;
    }

    free(node->childrens);
    free(node->keys);
    free(node);
}

//B树创建
void btree_create(btree *T, int degree)
{
    T->degree = degree;
    btree_node *node = btree_create_node(degree, 1);
    T->root = node;
}

//分割
void btree_split_child(btree *T, btree_node *node, int num)
{
    int _degree = T->degree;    //阶数

    btree_node *left = node->childrens[num];
    btree_node *right = btree_create_node(_degree, left->leaf);

    right->num = _degree - 1;

    for(int j = 0; j < _degree - 1; j++)
    {
        right->keys[j] = left->keys[j + _degree];
    }

    if(left->leaf == 0)
    {
        for(int i = 0; i < _degree; i++)
        {
            right->childrens[i] = left->childrens[i + _degree];
        }
    }

    left->num = _degree - 1;
    for(int j = node->num; j >= num + 1; j--)
    {
        node->childrens[j + 1] = node->childrens[j + _degree];
    }

    node->childrens[num + 1] = right;

    for(int j =node->num - 1; j >= num; j--)
    {
        node->keys[j + 1] = node->keys[j];
    }

    node->keys[num] = left->keys[_degree - 1];
    node->num += 1;
}

//
void btree_insert_nonfull(btree *T, btree_node *node, KEY_VALUE key)
{
    int _num = node->num - 1;

    if(node->leaf == 1)
    {
        while(_num >= 0 && node->keys[_num] > key)
        {
            node->keys[_num + 1] = node->keys[_num];
            _num--;
        }

        node->keys[_num + 1] = key;
        node->num += 11;
    }
    else
    {
        while(_num >= 0 && node->keys[_num] > key)
        {
            _num--;
        }

        if(node->childrens[_num + 1]->num == 2 * (T->degree) - 1)
        {
            btree_split_child(T, node, _num + 1);
            if(key > node->keys[_num + 1])
            {
                _num++;
            }
        }

        btree_insert_nonfull(T, node->childrens[_num + 1], key);
    }
}

//插入
void btree_insert(btree *T, KEY_VALUE key)
{
    btree_node *root = T->root;
    if(root->num == 2 * T->degree - 1)
    {
        btree_node *node = btree_create_node(T->degree, 0);
        T->root = node;

        node->childrens[0] = root;

        btree_split_child(T, node, 0);

        int i = 0;
        if(node->keys[0] < key)
        {
            i++;
        }
        
    }
}

void btree_merge(btree *T, btree_node *node, int idx)
{
    btree_node *left = node->childrens[idx];
    btree_node *right = node->childrens[idx + 1];

    left->keys[T->degree - 1] = node->keys[idx];
    for(int i = 0; i < T->degree - 1; i++)
    {
        left->keys[T->degree + i] = right->keys[i];
    }

    if(!left->leaf)
    {
        for(int i = 0; i< T->degree; i++)
        {
            left->childrens[T->degree + i] = right->childrens[i];
        }
    }

    left->num += T->degree;

    btree_destory_node(right);

    int i = 0;
    for(i = idx + 1; i < node->num; i++)
    {
        node->keys[i - 1] = node->keys[i];
        node->childrens[i] = node->childrens[i + 1];
    }

    node->childrens[i + 1] = NULL;
    node->num -= 1;

    if(node->num == 0)
    {
        T->root = left;
        btree_destory_node(node);
    }
}


void btree_delete_key(btree *T, btree_node *node, KEY_VALUE key)
{
    if(node == NULL)
    {
        return ;
    }

    int idx = 0;

    while(idx < node->num && key > node->keys[idx])
    {
        idx++;
    }

    if(idx < node->num && key == node->keys[idx])
    {
        if(node->leaf)
        {
            for(int i = idx; i < node->num - 1; i++)
            {
                node->keys[i] = node->keys[i + 1];
            }

            node->keys[node->num - 1] = 0;
            node->num--;

            if(node->num == 0)
            {
                free(node);
                T->root = NULL;
            }

            return ;
        }
        else if(node->childrens[idx]->num >= T->degree)
        {
            btree_node *left = node->childrens[idx];
            node->keys[idx] = left->keys[left->num - 1];

            btree_delete_key(T, left, left->keys[left->num - 1]);
        }
        else if(node->childrens[idx + 1]->num > T->degree)
        {
            btree_node *right = node->childrens[idx + 1];
            node->keys[idx] = right->keys[0];

            btree_delete_key(T, right, right->keys[0]);
        }
        else
        {
            btree_merge(T, node, idx);
            btree_delete_key(T, node->childrens[idx], key);
        }
    }
    else
    {
        btree_node *child = node->childrens[idx];
        if(child == NULL)
        {
            printf("Cannot del key = %d\n", key);
            return ;
        }

        if(child->num == T->degree - 1)
        {
            btree_node *left = NULL;
            btree_node *right = NULL;
            if(idx - 1 >= 0)
            {
                left = node->childrens[idx - 1];
            }
            if(idx + 1 <= node->num)
            {
                right = node->childrens[idx + 1];
            }

            if((left && left->num >= T->degree) || (right && right->num >= T->degree))
            {
                int richR = 0;
                if(right)
                {
                    richR = 1;
                }

                if(left && right)
                {
                    richR = (right->num > left->num) ? 1 : 0;
                }

                if(left && right->num >= T->degree && richR)
                {
                    child->keys[child->num] = node->keys[idx];
                    child->childrens[child->num + 1] = right->childrens[0];
                    child->num++;

                    node->keys[idx] = right->keys[0];
                    for(int i = 0; i < right->num - 1; i++)
                    {
                        right->keys[i] = right->keys[i + 1];
                        right->childrens[i] = right->childrens[i + 1];
                    }

                    right->keys[right->num - 1] = 0;
                    right->childrens[right->num - 1] = right->childrens[right->num];
                    right->childrens[right->num] = NULL;
                    right->num--;
                }
                else
                {
                    for(int i = child->num; i > 0; i--)
                    {
                        child->keys[i] = child->keys[i - 1];
                        child->childrens[i + 1] = child->childrens[i];
                    }

                    child->childrens[1] = child->childrens[0];
                    child->childrens[0] = left->childrens[left->num];
                    child->keys[0] = node->keys[idx - 1];
                    child->num++;

                    left->keys[left->num - 1] = 0;
                    left->childrens[left->num] = NULL;
                    left->num--;
                }
            }
            else if((!left || left->num == T->degree - 1)
                        && (!right || right->num == T->degree - 1))
            {
                if(left && left->num == T->degree - 1)
                {
                    btree_merge(T, node, idx - 1);
                    child = left;
                }
                else if(right && right->num == T->degree - 1)
                {
                    btree_merge(T, node, idx);
                }
            }
        }
    }
    
}

//删除
void btree_delete(btree *T, KEY_VALUE key)
{
    if(!T->root)
    {
        return ;
    }

    
}

//遍历
void btree_traverse(btree_node *node)
{
    int i = 0;
    for(i = 0; i < node->num; i++)
    {
        if(node->leaf == 0)
        {
            btree_traverse(node->childrens[i]);
        }
        printf("2%c", node->keys[i]);
    }

    if(node->leaf == 0)
    {
        btree_traverse(node->childrens[i]);
    }
}

void btree_print(btree *T, btree_node *node, int layer)
{
    btree_node *_node = node;
    if(_node)
    {
        printf("\nlayer = %d keynum = %d is_leaf = %d\n", layer, _node->num, _node->leaf);

        for(int i = 0; i < node->num; i++)
        {
            printf("2%c", _node->keys[i]);
        }
        printf("\n");

        layer++;
        for(int i = 0; i <= _node->num; i++)
        {
            if(_node->childrens[i])
            {
                btree_print(T, _node->childrens[i], layer);
            }
        }
    }
    else
    {
        printf("empty\n");
    }
}


//搜索
int btree_search(btree_node *node, int low, int high, KEY_VALUE key)
{
    int mid = 0;
    if(low > high || low < 0 || high < 0)
    {
        return -1;
    }

    while(low <= high)
    {
        mid = (high + low)/2 ;
        if(key > node->keys[mid])
        {
            low = mid + 1;
        }
        else
        {
            high = mid - 1;
        }
    }

    return low;
}

int main()
{
    btree T = {0};

    btree_create(&T ,DEGREE);
    //srand(48);

    char key[26] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    for(int i = 0; i < 26; i++)
    {
        printf("%c", key[i]);
        btree_insert(&T, key[i]);
    }

    btree_print(&T, T.root, 0);

    for(int j = 0; j < 26; j++)
    {
        printf("\n-----------------------------\n");
        btree_delete(&T, key[26 - j]);
        btree_print(&T, T.root, 0);
    }
}