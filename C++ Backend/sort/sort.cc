#include <stdio.h>

#define ARRAY_LENGTH 16

void shell_sort(int* array_data, int length)
{
    int temp = 0;
    int j = 0, i = 0;
    int gap = 0;
    for(gap = length/2; gap >= 1; gap = gap/2)
    {
        for(i = gap; i < length; i++)
        {
            temp = array_data[i];
            for(j = i - gap; j >= 0 && temp < array_data[j]; j = j - gap)
            {
                array_data[j + gap] = array_data[j];
            }
            array_data[j + gap] = temp;
        }
    }
}

void sort(int* array_data, int* temp, int start, int middle, int end)
{
    int _start = start;
    int _middle = middle + 1;
    int tmp_start = start;

    while(_start <= middle && _middle <= end)
    {
        if(array_data[_start] > array_data[_middle])
        {
            temp[tmp_start++] = array_data[_middle++];
        }
        else
        {
            temp[tmp_start++] = array_data[_start++];
        }
    }

    while(_start <= middle)
    {
        temp[tmp_start++] = array_data[_start++];
    }
    
    while(_middle <= end)
    {
        temp[tmp_start++] = array_data[_middle++];
    }

    for(_start = start; _start <= end; _start++)
    {
        array_data[_start] = temp[_start];
    }
}

void merge_sort(int* array_data, int* temp, int start, int end)
{
    int middle;
    if(start < end)
    {
        middle = start + (end - start)/2;
        merge_sort(array_data, temp, start, middle);
        merge_sort(array_data, temp, middle + 1, end);

        sort(array_data, temp, start, middle, end);
    }
}

void _quick_sort(int* array_data, int left, int right)
{
    if(right <= left)
    {
        return ;
    }

    int i = left;
    int j = right;
    int key = array_data[left];

    while(i < j)
    {
        while(i < j && key <= array_data[j])
        {
            j--;
        }
        array_data[i] = array_data[j];

        while(i < j && key >= array_data[i])
        {
            i++;
        }
        array_data[j] = array_data[i];
    }

    array_data[j] = key;

    _quick_sort(array_data, left, i - 1);
    _quick_sort(array_data, i + 1, right);
}

void quick_sort(int* array_data, int length)
{
    _quick_sort(array_data, 0, length - 1);
}


int main(int argc, char** argv)
{
    int array_data[ARRAY_LENGTH] = {34, 54, 78, 21, 23, 56, 93, 45, 22, 33, 55, 77, 22, 100, 89, 67};

    //shell_sort(array_data, ARRAY_LENGTH);

    int temp[ARRAY_LENGTH] = {0};
    //merge_sort(array_data, temp, 0, ARRAY_LENGTH - 1);

    quick_sort(array_data, ARRAY_LENGTH);
    for (int i = 0; i < ARRAY_LENGTH; i++)
    {
        printf("%4d", array_data[i]);
    }
    return 0;
}
